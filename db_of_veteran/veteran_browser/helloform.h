#ifndef HELLOFORM_H
#define HELLOFORM_H

#include <QWidget>
#include <QUrl>
#include <QDesktopServices>

#include "mainwindow.h"
#include "helpdialog.h"

namespace Ui {
    class HelloForm;
}

class HelloForm : public QWidget
{
    Q_OBJECT

public:
    explicit HelloForm(QWidget *parent = 0);
    ~HelloForm();

private slots:
    void showDatabase();
    void showAbout();
    void playVideo();
    void openAlbum();

private:
    Ui::HelloForm *ui;
    static MainWindow *w;
};

#endif // HELLOFORM_H
