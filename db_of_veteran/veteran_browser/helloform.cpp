#include "helloform.h"
#include "ui_helloform.h"

HelloForm::HelloForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HelloForm)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Window | Qt::WindowCloseButtonHint);
    connect(ui->tbDatabase, SIGNAL(clicked()), this, SLOT(showDatabase()));
    connect(ui->tbAbout, SIGNAL(clicked()), this, SLOT(showAbout()));
    connect( ui->tbClip, SIGNAL(clicked()), this, SLOT(playVideo()));
    connect( ui->tbAlbum, SIGNAL(clicked()), this, SLOT(openAlbum()));
}

HelloForm::~HelloForm()
{
    delete ui;
}

MainWindow* HelloForm::w = 0;

void HelloForm::showDatabase()
{
    if(w == 0)
        w = new MainWindow;
    w->showFullScreen();
}

void HelloForm::showAbout()
{
    HelpDialog dlg;
    dlg.exec();
}

void HelloForm::playVideo()
{
    QDesktopServices::openUrl(QUrl::fromLocalFile(QApplication::applicationDirPath () + "/New_film.wmv"));
}

void HelloForm::openAlbum()
{
    QDesktopServices::openUrl(QUrl::fromLocalFile(QApplication::applicationDirPath () + "/startCD.exe"));
}
