#include <QtGui/QApplication>
#include <QtSql>
#include <QDebug>
#include <QTextBrowser>

#include "helloform.h"

static bool createConnection()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QApplication::applicationDirPath() + "/" + "database.db");
    if (!db.open()) {
        qDebug() << "Fail, to connect database";
        return false;
    }
    return true;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.addLibraryPath(QString("./plugins/")); // ��� ���� ������ ���� ������ �� ���������
    if (!createConnection())
        return 1;

    HelloForm hw;
    hw.show();
//    w.show();
//    // Your HTML code
//    QString html;
//    html = "<html><head>"
//           "<link rel='stylesheet' type='text/css' href='format.css'>"
//           "</head><body>"
//           "Your HTML code with tags, which have classes or ids. For example "
//           "<span class='red'>this text is colored red</span>.<br/>"
//           "And you can also display images: <img src=':/ico/znak.png'><br/>"
//           "Combine css and images: <span id='bgimage'>foo bar</span>"
//           "</body></html>";

//    // Your CSS code
//    QString css;
//    css  = "span.red { color:#DE0000; } "
//           "span#bgimage { background-image: url(':/ico/lenta.png'); } "
//           "img {float:left}";

//    // Crate a QTextDocument with the defined HTML, CSS and the images
//    QTextDocument *doc = new QTextDocument;

//    /*
//     * This shows how to bind the images, where the name for QUrl is the same name
//     * which you use in your HTML and CSS. In QPixmap just use the normal syntax of
//     * the Qt resource system.
//     */
//    doc->addResource( QTextDocument::ImageResource, QUrl( ":/ico/znak.png" ), QPixmap( ":/ico/znak.png" ) );
//    doc->addResource( QTextDocument::ImageResource, QUrl( ":/ico/lenta.png" ), QPixmap( ":/ico/lenta.png" ) );

//    /*
//     * And now bind the css, which you have defined in the QString css.
//     */
//    doc->addResource( QTextDocument::StyleSheetResource, QUrl( "format.css" ), css );
//    doc->setHtml( html ); // binds the HTML to the QTextDocument

//    /*
//     * This QTextDocument can now set to any QTextBrowser.
//     */
//    QTextBrowser *tb = new QTextBrowser( 0 );
//    tb->setDocument( doc );
//    tb->show();

    return a.exec();
}
