#-------------------------------------------------
#
# Project created by QtCreator 2011-04-10T21:17:00
#
#-------------------------------------------------

QT       += core gui sql

TARGET = veteran_browser
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    findlineedit.cpp \
    helpdialog.cpp \
    helloform.cpp

HEADERS  += mainwindow.h \
    findlineedit.h \
    helpdialog.h \
    helloform.h

FORMS    += mainwindow.ui \
    helpdialog.ui \
    helloform.ui

RESOURCES += \
    icons.qrc
