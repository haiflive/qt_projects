#ifndef FINDLINEEDIT_H
#define FINDLINEEDIT_H

#include <QLineEdit>
#include <QKeyEvent>

class FindLineEdit : public QLineEdit
{
    Q_OBJECT
public:
    explicit FindLineEdit(QWidget *parent = 0);
    QString getWriteStringForComliter();

private:
    void keyPressEvent(QKeyEvent *e);
    QString StringForComliter;

};

#endif // FINDLINEEDIT_H
