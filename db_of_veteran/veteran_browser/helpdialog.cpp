#include "helpdialog.h"
#include "ui_helpdialog.h"

HelpDialog::HelpDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HelpDialog)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Drawer);
}

HelpDialog::~HelpDialog()
{
    delete ui;
}
