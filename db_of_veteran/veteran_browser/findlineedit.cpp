#include "findlineedit.h"

FindLineEdit::FindLineEdit(QWidget *parent) :
    QLineEdit(parent)
{
}

QString FindLineEdit::getWriteStringForComliter()
{
    return StringForComliter;
}

void FindLineEdit::keyPressEvent(QKeyEvent *e)
{
    QLineEdit::keyPressEvent(e);
    if(!(e->key() == Qt::Key_Return || e->key() == Qt::Key_Enter))
        StringForComliter = text();
}
