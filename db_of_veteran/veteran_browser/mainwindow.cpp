#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    lblImage( new QLabel)
{
    setWindowTitle(tr("���� ������ �� ���������"));
    ui->setupUi(this);
    connect(ui->textBrowser,SIGNAL(anchorClicked(QUrl)),this,SLOT(tbLinkClicked(QUrl)));
    modelTableCity =  new QSqlTableModel;
    modelTableCity->setTable("cites");
    modelTableCity->select();
    modelTableUser = new QSqlTableModel;
    modelTableUser->setTable("abonents");
    modelTableUser->setSort(2,Qt::AscendingOrder);
    modelTableUser->select();
    modelTableGallery = new QSqlTableModel;
    modelTableGallery->setTable("gallery");
    modelTableGallery->select();
/* START Default global Styles */
    QFont serifFont("Times", 14);
    formatDefaultPrototype.setFont(serifFont);

    QFont linkFont(serifFont);
    formatLinkPrototype.setAnchor(true);
    linkFont.setUnderline(true);
    formatLinkPrototype.setFont(linkFont);

    QFont titleFont("Times", 28);
    titleFont.setBold(true);
    formatTitlePrototype.setFont(titleFont);

    formatImagePrototype.setAnchor(true);
//    formatImagePrototype.setWidth(150);
    formatImagePrototype.setHeight(100);
/* END Default global Styles */
    cityID = 0;
    userID = 0;

    QCompleter *completer = new QCompleter(modelTableUser);
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    completer->setCompletionColumn(2);
    completer->setMaxVisibleItems(10);

    connect(completer, SIGNAL(activated(QModelIndex)), this, SLOT(findUserSelected(QModelIndex)));
    connect(ui->lblUserBarMain, SIGNAL(linkActivated(QString)), this, SLOT(onClickGoToHome()));
    connect(ui->lblUserBarCity, SIGNAL(linkActivated(QString)), this, SLOT(onClickGoToCity(QString)));
    connect(ui->tbHelp, SIGNAL(clicked()), this, SLOT(showHelp()));
    connect(ui->tbPrint, SIGNAL(clicked()), this, SLOT(printThisDocument()));
    ui->leFindUser->setCompleter(completer);
    document = new QTextDocument;

//    QString html;
//    html = "<html><head>"
//           "<link rel='stylesheet' type='text/css' href='format.css'>"
//           "</head><body>"
//           "Your HTML code with tags, which have classes or ids. For example "
//           "<span class='red'>this text is colored red</span>.<br/>"
//           "And you can also display images: <img src=':/ico/znak.png'><br/>"
//           "Combine css and images: <span id='bgimage'>foo bar</span>"
//           "</body></html>";
//    QString css;
//    css  = "span.red { color:#DE0000; } "
//           "span#bgimage { background-image: url(':/ico/lenta.png'); } "
//           "img {float:left}";
//    document->addResource( QTextDocument::ImageResource, QUrl( ":/ico/znak.png" ), QPixmap( ":/ico/znak.png" ) );
//    document->addResource( QTextDocument::ImageResource, QUrl( ":/ico/lenta.png" ), QPixmap( ":/ico/lenta.png" ) );
//    document->addResource( QTextDocument::StyleSheetResource, QUrl( "format.css" ), css );
//    document->setHtml( html ); // binds the HTML to the QTextDocument

//    QString result;
//    result = "<html><head>"
//                "<link rel='stylesheet' type='text/css' href='format.css'>"
//                "</head><body>";

//    QFile file(":/tmpl/userlist.htm");
//    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
//        return;

////    QString tamplateUserList;
//    QTextStream in(&file);
//    while (!in.atEnd()) {
//        QString line = in.readLine();
//        tamplateUserList.append(line);
//    }

//    QString css;
//    css  =
////           "img { border:0px; border-style:solid;border-color:#F0F0F0; width:150; padding:4px;margin:0px;background-color:#F0F0F0; float:none;}"
////           "span#fio {float:none}"
////           "span#image { border:0px; border-style:solid;border-color:#F0F0F0; width:150; padding:4px;margin:0px;background-color:#F0F0F0;float:none; } "
//           ".userBlock {border:0px; border-style:solid;border-color:#F0F0F0; width:150; padding:4px;margin:0px;background-color:#F0F0F0; float:left;}"
//           ;
////    document->addResource( QTextDocument::ImageResource, QUrl( ":/ico/lenta.png" ), QPixmap( ":/ico/lenta.png" ) );
//    document->addResource( QTextDocument::StyleSheetResource, QUrl( "format.css" ), css );

//    QString userBlock(tamplateUserList);
//    userBlock.replace("[url]", "#");
//    userBlock.replace("[img]", ":/ico/znak.png");
//    userBlock.replace("[fio]", "fio");
//    result.append(userBlock);
//    result.append(userBlock);
//    result.append(userBlock);
//    document->setHtml(result);

    cursor = new QTextCursor(document);

    ui->textBrowser->setDocument(document);
    getTextBrowserText();
}

MainWindow::~MainWindow()
{
    delete lblImage;
    delete ui;
}

void MainWindow::tbLinkClicked(const QUrl& url)
{
    qDebug() << url.toString().remove(0,url.toString().lastIndexOf("#")+1)
             << "->"
             << url.toString().split("-", QString::SkipEmptyParts);
    QApplication::setOverrideCursor(Qt::WaitCursor);
    QStringList list = url.toString().remove(0,url.toString().lastIndexOf("#")+1).split("-", QString::SkipEmptyParts);
    cityID = userID = -1;
    if(list.size() > 1)
    {
        bool ok,ok2;
        list.at(0).toInt(&ok);
        list.at(1).toInt(&ok2);
        if(ok && ok2)
        {
            cityID = list.at(0).toInt();
            userID = list.at(1).toInt();
        }
    }
    if(list.size() > 0)
        imageID = url.toString().remove(0,url.toString().lastIndexOf("#")+1);
    getTextBrowserText();
    QApplication::restoreOverrideCursor();
}

void MainWindow::findUserSelected(const QModelIndex &index)
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    //! ��� sqllite, ����������������� �����, upper �� ��������
    // �������� ��������
    QString strFind = ui->leFindUser->getWriteStringForComliter();
    strFind[0] = strFind[0].toUpper();
//    qDebug() << strFind;
    modelTableUser->setFilter(QString("fio LIKE '%1%'").arg(strFind));
//    qDebug() << ui->leFindUser->getWriteStringForComliter() << index.row() << "=" << modelTableUser->record(index.row()).value("id") << modelTableUser->record(index.row()).value("fio");

    cityID = modelTableUser->record(index.row()).value("idcity").toInt();
    userID = modelTableUser->record(index.row()).value("id").toInt();
    modelTableUser->setFilter("");
        getTextBrowserText();
    QApplication::restoreOverrideCursor();
}

void MainWindow::onClickGoToHome()
{
    cityID = userID = 0;
    getTextBrowserText();
}

void MainWindow::onClickGoToCity(const QString& strurl)
{
    QUrl url(strurl);
    /*emit */tbLinkClicked(url);
}

void MainWindow::showHelp()
{
    HelpDialog dlg;
    dlg.exec();
}

void MainWindow::printThisDocument()
{
    QPrinter printer(QPrinter::HighResolution);
    printer.setFullPage(true);
    QPrintDialog printDialog(&printer);
    printDialog.setEnabledOptions(QAbstractPrintDialog::PrintPageRange);
    printDialog.setPrintRange(QAbstractPrintDialog::AllPages);
    if (ui->textBrowser->textCursor().hasSelection()){
            printDialog.addEnabledOption(QAbstractPrintDialog::PrintSelection);
    }
    printDialog.setWindowTitle(tr("������"));

    if (printDialog.exec() == QDialog::Accepted)
            ui->textBrowser->print(&printer);
}

void MainWindow::getTextBrowserText()
{
    if(cityID == -1 && userID == -1)
        showPhoto();
    else {
        document->clear();
        if(cityID > 0 && userID > 0)
             getUserInfo();
        else if(cityID > 0)
             getUserList();
        else getCityList();
    }
}

void MainWindow::getCityList()
{
    ui->lblUserBarCity->setText("");
    ui->lblMarker->hide();
    QTextCharFormat format(formatLinkPrototype);
//    format.setBackground(QBrush(QImage(":/ico/Printer.png")));
    format.setFontPointSize(28);
//    format.setForeground(QBrush(QColor(60,210,255)));
//    QTextTableFormat tableFormat;
//    tableFormat.setAlignment(Qt::AlignHCenter);
////    tableFormat.setBackground(QBrush(QImage(":/ico/lenta.png")));
////    tableFormat.setForeground(QBrush(QColor(200,200,100),Qt::Dense6Pattern));
//    tableFormat.setBorder(0);
//    tableFormat.setWidth(480);
//    tableFormat.setHeight(50);
////    tableFormat.setMargin(5);
//    tableFormat.setCellPadding(20);
//    tableFormat.setCellSpacing(-10);

    for( int i = 0; i < modelTableCity->rowCount(); ++i )
    {
        cursor->insertBlock();
        format.setAnchorHref("#" + modelTableCity->record(i).value(0).toString() + "-0");
//        cursor->insertTable(1,1,tableFormat);
        ui->textBrowser->setAlignment(Qt::AlignHCenter);
        cursor->insertText(modelTableCity->record(i).value(1).toString(), format);
//        cursor->movePosition(QTextCursor::End);
    }
//    ui->textBrowser->setAlignment(Qt::AlignLeft);
}

void MainWindow::getUserList()
{
    modelTableCity->setFilter(QString("id = %1").arg(cityID));
    if(!modelTableCity->record(0).isEmpty())
    {
        ui->lblUserBarCity->setText(QString("<a href=\"#%1-0\"><span style=\" font-size:12pt; text-decoration: underline; color:#0000ff;font-weight: bolder;\">%2</span></a>").arg(modelTableCity->record(0).value(0).toString(),modelTableCity->record(0).value(1).toString()));
        ui->lblMarker->show();
    }
    modelTableCity->setFilter("");
    modelTableUser->setFilter(QString("idcity = %1").arg(cityID));

    QTextCharFormat format(formatLinkPrototype);
    QTextImageFormat formatImg(formatImagePrototype);
    QTextTableFormat tableFormat;
    tableFormat.setAlignment(Qt::AlignHCenter);
    tableFormat.setBorder(0);
    tableFormat.setCellSpacing(10);

    const int cellsCount = 4;
    int rowCount = (modelTableUser->rowCount() / cellsCount) + 1;
    QTextDocument doc;
    QTextCursor cursorDoc(&doc);
    cursorDoc.insertTable(rowCount,cellsCount,tableFormat);

//    qDebug() << tamplateUserList;

    for( int i = 0; i < modelTableUser->rowCount(); ++i )
    {
        formatImg.setAnchorHref("#" + modelTableUser->record(i).value(1).toString() + "-" + modelTableUser->record(i).value(0).toString());

        if(QFile::exists(QApplication::applicationDirPath () +
                         "/images/" +
                         modelTableUser->record(i).value(3).toString()) && !modelTableUser->record(i).value(3).toString().isEmpty())
        {
        formatImg.setName(QApplication::applicationDirPath () +
                          "/images/" +
                          modelTableUser->record(i).value(3).toString());
//        qDebug() << "true";
        }
        else formatImg.setName(":/ico/stock_people.png");
//        if(i % cellsCount == 0)
//            cursor->insertTable(rowCount,cellsCount,tableFormat);
        cursorDoc.insertImage(formatImg/*,QTextFrameFormat::InFlow*/);
        cursorDoc.insertBlock();

        format.setAnchorHref("#" + modelTableUser->record(i).value(1).toString() + "-" + modelTableUser->record(i).value(0).toString());
        cursorDoc.insertText(modelTableUser->record(i).value(2).toString(), format);
        if(i % cellsCount != cellsCount - 1)
            cursorDoc.movePosition(QTextCursor::NextCell);
        else cursorDoc.movePosition(QTextCursor::NextRow);
    }
//    cursor->insertHtml(result);
//    document->setHtml(result);
//    cursorDoc.movePosition(QTextCursor::End);
    cursorDoc.insertBlock();
    document->setHtml(doc.toHtml());
    modelTableUser->setFilter("");
}

void MainWindow::getUserInfo()
{
    modelTableCity->setFilter(QString("id = %1").arg(cityID));
    if(!modelTableCity->record(0).isEmpty())
    {
        ui->lblUserBarCity->setText(QString("<a href=\"#%1-0\"><span style=\" font-size:12pt; text-decoration: underline; color:#0000ff;\">%2</span></a>").arg(modelTableCity->record(0).value(0).toString(),modelTableCity->record(0).value(1).toString()));
        ui->lblMarker->show();
    }
    modelTableCity->setFilter("");

    QTextCharFormat format(formatTitlePrototype);
    QTextImageFormat formatImg(formatImagePrototype);
    QTextTableFormat tableFormat;
    tableFormat.setAlignment(Qt::AlignLeft);
    tableFormat.setBorder(0);

    modelTableUser->setFilter(QString("id = %1").arg(userID));
    if(!modelTableUser->record(0).isEmpty())
    {
        formatImg.setAnchorHref("#" + modelTableUser->record(0).value(3).toString());
        if(QFile::exists(QApplication::applicationDirPath () +
                         "/images/" +
                         modelTableUser->record(0).value(3).toString()) && !modelTableUser->record(0).value(3).toString().isEmpty())
        formatImg.setName(QApplication::applicationDirPath () +
                       "/images/" +
                       modelTableUser->record(0).value(3).toString());
        else formatImg.setName(":/ico/stock_people.png");
//        qDebug() << userID << modelTableUser->record(0).value(3) << modelTableUser->record(0).value(0);
//        formatImg.setWidth(150);
        cursor->insertTable(1,2,tableFormat);
        cursor->insertImage(formatImg);
        cursor->movePosition(QTextCursor::NextCell);
        cursor->insertHtml("&nbsp;&nbsp;");
        cursor->insertText(modelTableUser->record(0).value(2).toString(), format);
        cursor->movePosition(QTextCursor::End);

        cursor->insertHtml(modelTableUser->record(0).value(4).toString());
        cursor->insertBlock();

        modelTableGallery->setFilter(QString("idabonent = %1").arg(userID));

//        cursor->insertHtml(tr("Gallery"), format);
        cursor->insertBlock();

        for( int i = 0; i < modelTableGallery->rowCount(); ++i )
        {
         formatImg.setAnchorHref("#" + modelTableGallery->record(i).value(2).toString());
         formatImg.setName(QApplication::applicationDirPath () +
                           "/images/" +
                           modelTableGallery->record(i).value(2).toString());
//         formatImg.setWidth(150);
         cursor->insertHtml("&nbsp;&nbsp;");
         cursor->insertImage(formatImg);
//         cursor->insertBlock();
        }
    }
    modelTableUser->setFilter("");
    modelTableGallery->setFilter("");
}

void MainWindow::showPhoto()
{
    //��������� �� ������� ��������� � ���������������?
    // ���������� ���� � ��������� ���������?
    QString imgFile = imageID;
    imgFile.prepend("/images/");
    imgFile.prepend(QApplication::applicationDirPath());
    if ( QFile::exists( imgFile )  && !imageID.isEmpty())
    {
        QPixmap pixmap( imgFile );
        pixmap = pixmap.scaled(800,600,Qt::KeepAspectRatio);
        lblImage->resize(pixmap.size());
        lblImage->setPixmap(pixmap);
    //    lblImage->showMaximized();
        lblImage->setWindowModality(Qt::ApplicationModal);
        lblImage->setWindowTitle(imgFile);
        lblImage->show();
    }
    imageID.clear();
}

//setAnchorHref() should only contain the href, not any html tags.

// I didn't test this but try adjusting something like this to your needs:

// QTextCursor addLink(QTextCursor cursor, const QString &url, QString title = QString()) {
// if(title.isEmpty()) title = url;
// QTextCharFormat original = cursor.charFormat();
// QTextCharFormat format;
// format.setAnchor(true);
// format.setAnchorHref(url);
// cursor.insertText(title, format);
// cursor.setFormat(original);
// return cursor;
// }

// QTextEdit *te = ...
// addLink(te->textCursor(), "http://www.qtcentre.org", "QtCentre");

// For adding an image instead of text you will probably want to use QTextCursor::insertImage() instead of insertText():

// QTextImageFormat format;
// format.setAnchor(true);
// format.setAnchorHref(url);
// format.setName(imageName);
// cursor.insertImage(format);
