#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUrl>
#include <QDebug>
#include <QSqlTableModel>
#include <QCompleter>
#include <QSqlRecord>
#include <QModelIndex>
#include <QKeyEvent>
#include <QTextDocument>
#include <QTextCharFormat>
#include <QFile>
#include <QLabel>
#include <QTextTable>
#include <QPrinter>
#include <QPrintDialog>

#include "helpdialog.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void tbLinkClicked(const QUrl& url);
    void findUserSelected(const QModelIndex & index);
    void onClickGoToHome();
    void onClickGoToCity(const QString& url);
    void showHelp();
    void printThisDocument();

private:
    void getTextBrowserText();
    void getCityList();
    void getUserList();
    void getUserInfo();
    void showPhoto();

    Ui::MainWindow *ui;
    QSqlTableModel* modelTableCity;
    QSqlTableModel* modelTableUser;
    QSqlTableModel* modelTableGallery;
    QTextDocument *document;
    QTextCursor *cursor;

    QTextCharFormat formatDefaultPrototype;
    QTextCharFormat formatLinkPrototype;
    QTextCharFormat formatTitlePrototype;
    QTextImageFormat formatImagePrototype;

    QString tamplateUserList;

    int cityID;
    int userID;
    QString imageID;
    QLabel* lblImage;
};

#endif // MAINWINDOW_H
