#include "modelgalleryicon.h"

modelGalleryIcon::modelGalleryIcon()
{
    dbPath = QApplication::applicationDirPath();
}

QVariant modelGalleryIcon::data(const QModelIndex &index, int role) const
{
    if ( index.column() == 2 )
    {
        if ( Qt::DisplayRole == role )
        {
            return QString();
        }
        if ( role == Qt::DecorationRole )
        {
            QString imgFile = QSqlTableModel::data( index, Qt::DisplayRole ).toString();
            imgFile.prepend("/images/");
            imgFile.prepend(dbPath/*QApplication::applicationDirPath()*/);
            if ( !QFile::exists( imgFile )  || imgFile.isEmpty())
            {
                imgFile = ":/ico/not_found.png";
//                imgFile = "stock_people.png";
//                imgFile.prepend("/images/");
//                imgFile.prepend(QApplication::applicationDirPath());
            }
            QPixmap pixmap( imgFile );
            if(pixmap.isNull())
                return QVariant();
            pixmap = pixmap.scaledToHeight(90);

            return pixmap;
        }
//        if(role == Qt::SizeHintRole)
//        {
//            return pixmap.size();
//        }
    }
    return QSqlTableModel::data( index, role );
}

void modelGalleryIcon::setPath(const QString& str)
{
    dbPath = str;
}

//Qt::ItemFlags modelGalleryIcon::flags(const QModelIndex &index) const
//{
//    if (!index.isValid())
//        return Qt::ItemIsEnabled;

//    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
//}
