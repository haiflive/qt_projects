#-------------------------------------------------
#
# Project created by QtCreator 2011-03-06T01:01:59
#
#-------------------------------------------------

QT       += core gui sql

TEMPLATE = app
TARGET = veteran

# Input
HEADERS +=  dialogabout.h\
			mainwindow.h\
			modelgalleryicon.h
			
FORMS += dialogabout.ui\
		 mainwindow.ui
		 
SOURCES +=  dialogabout.cpp\
			main.cpp\
			mainwindow.cpp\
			modelgalleryicon.cpp
			
RESOURCES += \
	icons.qrc

TRANSLATIONS += veteran_ru.ts
