#include "mainwindow.h"
//#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui( new Ui::MainWindow ),
    hidenLEMapperPhoto( new QLineEdit),
    lblImage( new QLabel)
//    modelG( new modeGroup )
//    modelA( new modelAbonent )
{
    ui->setupUi(this);
    setupActions();

//    HomeDir = QDir::homePath() + "/";
//    HomeDir = QApplication::applicationDirPath ()  + "/";
//    qDebug() << HomeDir;
    ProgramTitle = tr("Veteran's Database");		// ��������� ���������

    // ����������� ������� ��� ���� (��������� � ������)
//    setWindowIcon( QIcon(":/images/icon.png") );
    setWindowTitle( ProgramTitle );

    readSettings();	// ��������������� ��������� �� ������� (��� ����� � Linux), ��� ���������� ������ �������� �� ���������, ���� ������ ���
    createConnection();		// ��������� ���� ����
//    modelG->setQuery("SELECT name, id FROM cites");
    modelG = new QSqlTableModel;
    modelG->setTable("cites");
    modelG->setEditStrategy(QSqlTableModel::OnRowChange);
    modelG->select();
    ui->lVCites->setModel(modelG);
    ui->lVCites->setModelColumn(1);

    ui->actionAddCity->setEnabled(true);
    connect( ui->lVCites, SIGNAL(clicked(QModelIndex)), this, SLOT(selectCity(QModelIndex)) );

//    modelA->setQuery("SELECT fio, id, img, description FROM abonents");
//    modelTableA->setTable("abonents");
    modelTableA = new QSqlTableModel;
    modelTableA->setTable("abonents");
    modelTableA->setFilter("1=2");
    modelTableA->setEditStrategy(QSqlTableModel::OnRowChange);
    modelTableA->select();

    ui->lVUsers->setModel(modelTableA);
    ui->lVUsers->setModelColumn(2);
    connect( ui->lVUsers, SIGNAL(clicked(QModelIndex)), this, SLOT(selectUser(QModelIndex)) );
    mapperA = new QDataWidgetMapper(this);
    mapperA->setModel(modelTableA);
    mapperA->setSubmitPolicy(QDataWidgetMapper::AutoSubmit);
    mapperA->addMapping(ui->lEfio, 2);
//    hidenMapperPhoto->show();
    mapperA->addMapping(hidenLEMapperPhoto,3);
    mapperA->addMapping(ui->tEDescription, 4);

    connect(ui->lVUsers->selectionModel(), SIGNAL(currentRowChanged(QModelIndex,QModelIndex)),
            mapperA, SLOT(setCurrentModelIndex(QModelIndex)));
//    connect(hidenLEMapperPhoto,SIGNAL(textChanged(QString)),this,SLOT(updatePhoto(QString)));
//    hidenLEMapperPhoto->show();

    modelGallery =  new modelGalleryIcon;
    modelGallery->setTable("gallery");
    modelGallery->setFilter("1=2");
    modelGallery->setPath(dbPath);
    modelGallery->setEditStrategy(QSqlTableModel::OnRowChange);
    modelGallery->select();
    ui->lVGallery->setModel(modelGallery);
    ui->lVGallery->setModelColumn(2);
    connect(ui->lVGallery, SIGNAL(clicked(QModelIndex)), this, SLOT(selectGallery(QModelIndex)));
//    setModified(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    saveAll();
    writeSettings();
//        // ���� � ���� ���� ������� ���������
//        if ( getModified() )
//        {
//                // ���������� ������ - ����� �� ��������� ��������� ����� ���������
//                int r = QMessageBox::warning( this, ProgramTitle,
//                                                tr("The table has been modified.%1"
//                                                "Do you want to save changes?").arg("\n"),
//                                                QMessageBox::Yes | QMessageBox::Default,
//                                                QMessageBox::No,
//                                                QMessageBox::Cancel | QMessageBox::Escape );
//                switch (r)
//                {
//                        case QMessageBox::Yes	: writeSettings(); /*saveFile();*/ event->accept(); break;	// ���� ����� ���������
//                        case QMessageBox::No	: event->accept(); break;                               // ���� �� ����� ���������
//                        default                 : event->ignore(); break;                               // ���� �� ����� ��������� ���������
//                }
//        }
//        else // ���� ��������� �� ����
//        {
//                event->accept(); // �� ������ ��������� ���������
//        }
}

/* SLOTS */
void MainWindow::AddCity()
{
    QSqlRecord record = modelG->record();
    record.setValue( "name", tr("city name"));
    modelG->insertRecord(modelG->rowCount(), record);

    ui->lVCites->setCurrentIndex(modelG->index(modelG->rowCount()-1,1));
    ui->lVCites->edit(ui->lVCites->currentIndex());
}

void MainWindow::DelCity()
{
    //! ��� �� ����� ������� ���� ������������� � �� ������� ��������������
//    modelG->removeRows(modelG->rowCount(),1,ui->lVCites->currentIndex());
    int row(ui->lVCites->currentIndex().row());

    int r = QMessageBox::warning( this, ProgramTitle,
                                    tr("Delete city, %1")
                                        .arg(modelG->record(row).value("name").toString()),
                                    QMessageBox::Yes,
                                    QMessageBox::No | QMessageBox::Default );
    switch (r)
    {
            case QMessageBox::Yes :
        for(int j = 0; modelTableA->rowCount() > j; ++j)
        {
//            ui->lVUsers->setCurrentIndex(modelTableA->index(j,2));
            modelGallery->setFilter(QString("idabonent = %1").arg(modelTableA->record(modelTableA->index(j,2).row()).value("id").toInt()));
            qDebug() << QString("idabonent = %1").arg(modelTableA->record(modelTableA->index(j,2).row()).value("id").toInt());
            for(int i = 0; modelGallery->rowCount() > i; ++i)
            {
                ui->lVGallery->setCurrentIndex(modelGallery->index(i,2));
                removePhoto();
//                qDebug() << i << j << modelGallery->record(ui->lVGallery->currentIndex().row()).value("filename").toString() ;
            }
            modelGallery->removeRows(0,modelGallery->rowCount());
        }
        modelTableA->removeRows(0,modelTableA->rowCount());
        modelG->removeRow(row);
//                ui->lVCites->setCurrentIndex(modelG->index(row-1,1));
                break;
            default					: break;								// ���� �� ����� ��������� ���������
    }
}

void MainWindow::EditCity()
{
    ui->lVCites->edit(ui->lVCites->currentIndex());
//    qDebug() << ui->lVCites->currentIndex().row();
}

void MainWindow::AddUser()
{
    QSqlRecord record = modelTableA->record();
    record.setValue( "idcity", modelG->record(ui->lVCites->currentIndex().row()).value("id") );
    record.setValue( "fio", tr("fio") );
    modelTableA->insertRecord(modelTableA->rowCount(), record);

    ui->lVUsers->setCurrentIndex(modelTableA->index(modelTableA->rowCount()-1,2));
    ui->lVUsers->edit(ui->lVUsers->currentIndex());

}

void MainWindow::EditUser()
{
    ui->lVUsers->edit(ui->lVUsers->currentIndex());
}

void MainWindow::DelUser()
{
    int row(ui->lVUsers->currentIndex().row());

    int r = QMessageBox::warning( this, ProgramTitle,
                                    tr("Delete user, %1")
                                        .arg(modelTableA->record(row).value("fio").toString()),
                                    QMessageBox::Yes,
                                    QMessageBox::No | QMessageBox::Default );
    switch (r)
    {
            case QMessageBox::Yes	:
                    for(int i = 0; modelGallery->rowCount() > i; ++i)
                    {
                        ui->lVGallery->setCurrentIndex(modelGallery->index(i,2));
                        removePhoto();
                    }
                    modelGallery->removeRows(0,modelGallery->rowCount());

                    modelTableA->removeRow(row);
//                    ui->lVUsers->setCurrentIndex(modelTableA->index(row-1,2));
                    emit updatePhoto("");
                break;
//            case QMessageBox::No	: event->accept(); break;
            default					: break;								// ���� �� ����� ��������� ���������
    }
}

void MainWindow::clipboardDataChanged()
{
#ifndef QT_NO_CLIPBOARD
    ui->actionEditorPaste->setEnabled(!QApplication::clipboard()->text().isEmpty());
#endif
}

void MainWindow::EditorBold()
{
    QTextCharFormat fmt;
    fmt.setFontWeight(ui->actionEditorBold->isChecked() ? QFont::Bold : QFont::Normal);
    mergeFormatOnWordOrSelection(fmt);
}

void MainWindow::EditorItalic()
{
    QTextCharFormat fmt;
    fmt.setFontItalic(ui->actionEditorItalic->isChecked());
    mergeFormatOnWordOrSelection(fmt);
}

void MainWindow::EditorUnderLine()
{
    QTextCharFormat fmt;
    fmt.setFontUnderline(ui->actionEditorUnderLine->isChecked());
    mergeFormatOnWordOrSelection(fmt);
}

//void MainWindow::EditorLeft()
//{

//}

//void MainWindow::EditorCenter()
//{

//}

//void MainWindow::EditorRight()
//{

//}

//void MainWindow::EditorJustify()
//{

//}

void MainWindow::currentCharFormatChanged(const QTextCharFormat &format)
{
    fontChanged(format.font());
    colorChanged(format.foreground().color());
}
void MainWindow::cursorPositionChanged()
{
    alignmentChanged(ui->tEDescription->alignment());
}

void MainWindow::textColor()
{
    QColor col = QColorDialog::getColor(ui->tEDescription->textColor(), this);
    if (!col.isValid())
        return;
    QTextCharFormat fmt;
    fmt.setForeground(col);
    mergeFormatOnWordOrSelection(fmt);
    colorChanged(col);
}

void MainWindow::textAlign(QAction *a)
{
    if (a == ui->actionEditorLeft)
        ui->tEDescription->setAlignment(Qt::AlignLeft | Qt::AlignAbsolute);
    else if (a == ui->actionEditorCenter)
        ui->tEDescription->setAlignment(Qt::AlignHCenter);
    else if (a == ui->actionEditorRight)
        ui->tEDescription->setAlignment(Qt::AlignRight | Qt::AlignAbsolute);
    else if (a == ui->actionEditorJustify)
        ui->tEDescription->setAlignment(Qt::AlignJustify);
}

void MainWindow::textSize(const QString &p)
{
    qreal pointSize = p.toFloat();
    if (p.toFloat() > 0) {
        QTextCharFormat fmt;
        fmt.setFontPointSize(pointSize);
        mergeFormatOnWordOrSelection(fmt);
    }
}

void MainWindow::textFamily(const QString &f)
{
    QTextCharFormat fmt;
    fmt.setFontFamily(f);
    mergeFormatOnWordOrSelection(fmt);
}

void MainWindow::textStyle(int styleIndex)
 {
     QTextCursor cursor = ui->tEDescription->textCursor();

     if (styleIndex != 0) {
         QTextListFormat::Style style = QTextListFormat::ListDisc;

         switch (styleIndex) {
             default:
             case 1:
                 style = QTextListFormat::ListDisc;
                 break;
             case 2:
                 style = QTextListFormat::ListCircle;
                 break;
             case 3:
                 style = QTextListFormat::ListSquare;
                 break;
             case 4:
                 style = QTextListFormat::ListDecimal;
                 break;
             case 5:
                 style = QTextListFormat::ListLowerAlpha;
                 break;
             case 6:
                 style = QTextListFormat::ListUpperAlpha;
                 break;
             case 7:
                 style = QTextListFormat::ListLowerRoman;
                 break;
             case 8:
                 style = QTextListFormat::ListUpperRoman;
                 break;
         }

         cursor.beginEditBlock();

         QTextBlockFormat blockFmt = cursor.blockFormat();

         QTextListFormat listFmt;

         if (cursor.currentList()) {
             listFmt = cursor.currentList()->format();
         } else {
             listFmt.setIndent(blockFmt.indent() + 1);
             blockFmt.setIndent(0);
             cursor.setBlockFormat(blockFmt);
         }

         listFmt.setStyle(style);

         cursor.createList(listFmt);

         cursor.endEditBlock();
     } else {
         // ####
         QTextBlockFormat bfmt;
         bfmt.setObjectIndex(-1);
         cursor.mergeBlockFormat(bfmt);
     }
 }

void MainWindow::textUpper()
{
    fontSize++;
    textSize(QString::number(getFontSize()));
}

void MainWindow::textLower()
{
    fontSize--;
    textSize(QString::number(getFontSize()));
}

//void MainWindow::New_Database()
//{
////    QString fileName = QFileDialog::getSaveFileName(
////                    HomeDir,
////                    "",
////                    this,
////                    "create file dialog"
////                    "Choose a filename to save under" );
//    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
//                               HomeDir,
//                               tr("Images (*.db)"));
////     if (QFile::exists(fileName) )
////    {
////  QString err = "File ";
////  err.append(fileName);
////  err.append(" already exists. Please choose a different name");
//// QMessageBox::information( this, ProgramTitle ,err);
//// return;
////    }
//    if (!fileName.isNull())
//    {
//        FileName = fileName;
//        createConnection();
//    QSqlQuery query("DROP TABLE IF EXISTS `abonents`"
//                    "CREATE TABLE `abonents` ("
//                      "`id` int(11) NOT NULL AUTO_INCREMENT,"
//                      "`fio` varchar(255) NOT NULL DEFAULT '',"
//                      "`img` varchar(255) DEFAULT NULL,"
//                      "`description` LONGTEXT"
//                      "PRIMARY KEY (`id`)"
//                    ");"

//                    "DROP TABLE IF EXISTS `cites`;"
//                    "CREATE TABLE `cites` ("
//                      "`id` int(11) NOT NULL AUTO_INCREMENT,"
//                      "`name` varchar(255) NOT NULL DEFAULT '',"
//                      "PRIMARY KEY (`id`)"
//                    ");"

//                    "DROP TABLE IF EXISTS `gallary`;"
//                    "CREATE TABLE `cites` ("
//                      "`id` int(11) NOT NULL AUTO_INCREMENT,"
//                      "`idabonent` int(11) DEFAULT '0',"
//                      "`filepath` varchar(255) NOT NULL DEFAULT '',"
//                      "PRIMARY KEY (`id`)"
//                    ");"
//                    );
//    query.exec();
//// db.create(fileName);
//// this->setCaption(applicationName+" - "+fileName);
//// populateStructure();
//// resetBrowser();
//// createTable();
//// fileCloseAction->setEnabled(true);
//// fileCompactAction->setEnabled(true);
//// editCreateTableAction->setEnabled(true);
//// editDeleteTableAction->setEnabled(true);
//// editModifyTableAction->setEnabled(true);
//// editCreateIndexAction->setEnabled(true);
//// editDeleteIndexAction->setEnabled(true);
//    }
//}

//void MainWindow::Open_Database()
//{
//    QString fileName;
//    fileName = QFileDialog::getOpenFileName(this,
//         tr("Open Database"), HomeDir, tr("Database (*.db *.sql)"));
//    FileName = fileName;
//    createConnection();

//        QSqlQuery *query = new QSqlQuery("CREATE TABLE sometable ("
//                                         "id INTEGER,"
//                                         "somedata TEXT", db);
//}

//void MainWindow::Save_Database()
//{
////    saveFile();
//    modelG->submitAll();
//    modelTableA->submitAll();
//    modelGallery->submitAll();
//}

//void MainWindow::Revert_Database()
//{
//    // ������ ��������� ���������
//}

void MainWindow::addPhoto()
{
    QString fileName;
    fileName = QFileDialog::getOpenFileName(this,
         tr("Open Image"), QDir::homePath(), tr("Images (*.bmp *.jpg *.jpeg *.jpe *.png *.tif *.tiff);; All files(*)"));
    if(!fileName.isEmpty())
    {
        QString tmpFileName;
        QStringList parts = fileName.split("/");
        tmpFileName = parts.at(parts.size()-1);
        QString imgFile;
        imgFile.prepend(tmpFileName);
        imgFile.prepend("/images/");
        imgFile.prepend(dbPath/*QApplication::applicationDirPath()*/);
        int i(0);
        while ( QFile::exists( imgFile ) )
        {
            imgFile.clear();
            imgFile.prepend(tmpFileName);
            imgFile.prepend("_");
            imgFile.prepend(QString::number(i));
            imgFile.prepend("/images/");
            imgFile.prepend(dbPath/*QApplication::applicationDirPath()*/);
            ++i;
        }
        QPixmap pixmap( fileName );
//        if(pixmap.isNull())
        pixmap = pixmap.scaledToWidth(1024,Qt::SmoothTransformation);
        pixmap.save(imgFile);
        QStringList parts2 = imgFile.split("/");
        imgFile.clear();
        imgFile = parts2.at(parts2.size()-1);

        QSqlRecord record = modelGallery->record();
        record.setValue( "idabonent", modelTableA->record(ui->lVUsers->currentIndex().row()).value("id") );
        record.setValue( "filename", imgFile );
        modelGallery->insertRecord(modelGallery->rowCount(), record);
    }
//    ui->lVGallery->setCurrentIndex(modelGallery->index(modelGallery->rowCount()-1,2));
}

void MainWindow::delPhoto()
{
    int row(ui->lVGallery->currentIndex().row());
    int r = QMessageBox::warning( this, ProgramTitle,
                                    tr("Delete photo, %1")
                                        .arg(modelGallery->record(row).value("filename").toString()),
                                    QMessageBox::Yes,
                                    QMessageBox::No | QMessageBox::Default );
    switch (r)
    {
            case QMessageBox::Yes	:
                removePhoto();
                modelGallery->removeRow(row);
                break;
            default					: break;								// ���� �� ����� ��������� ���������
    }
//    ui->lVGallery->setCurrentIndex(modelGallery->index(modelGallery->rowCount()-1,2));
}

void MainWindow::editPhoto()//user photo
{

    QString imgFile = modelGallery->record(ui->lVGallery->currentIndex().row()).value("filename").toString();

    QSqlRecord record = modelTableA->record(ui->lVUsers->currentIndex().row());
    record.setValue( "img", imgFile );
    modelTableA->setRecord(ui->lVUsers->currentIndex().row(),record);

//    QString fileName;
//    fileName = QFileDialog::getOpenFileName(this,
//         tr("Open Image"), QDir::homePath(), tr("Images (*.bmp *.jpg *.jpeg *.jpe *.png *.tif *.tiff);; All files(*)"));
//    if(!fileName.isEmpty())
//    {
//        QString tmpFileName;
//        QStringList parts = fileName.split("/");
//        tmpFileName = parts.at(parts.size()-1);
//        QString imgFile;
//        imgFile.prepend(tmpFileName);
//        imgFile.prepend("/images/");
//        imgFile.prepend(QApplication::applicationDirPath());
//        int i(0);
//        while ( QFile::exists( imgFile ) )
//        {
//            imgFile.clear();
//            imgFile.prepend(tmpFileName);
//            imgFile.prepend("_");
//            imgFile.prepend(QString::number(i));
//            imgFile.prepend("/images/");
//            imgFile.prepend(QApplication::applicationDirPath());
//            ++i;
//        }
//        QPixmap pixmap( fileName );
//        pixmap = pixmap.scaledToWidth(1024,Qt::SmoothTransformation);
//        pixmap.save(imgFile);
//        QStringList parts2 = imgFile.split("/");
//        imgFile.clear();
//        imgFile = parts2.at(parts2.size()-1);

//        QSqlRecord record = modelTableA->record(ui->lVUsers->currentIndex().row());
//        record.setValue( "img", imgFile );
//        modelTableA->setRecord(ui->lVUsers->currentIndex().row(),record);

//        //and add to gallery
//        QSqlRecord record2 = modelGallery->record();
//        record2.setValue( "idabonent", modelTableA->record(ui->lVUsers->currentIndex().row()).value("id") );
//        record2.setValue( "filename", imgFile );
//        modelGallery->insertRecord(modelGallery->rowCount(), record2);

//    //    ui->lVGallery->setCurrentIndex(modelGallery->index(modelGallery->rowCount()-1,2));

        emit updatePhoto(imgFile);
//    }
}

void MainWindow::openImageBrowser()
{
    //��������� �� ������� ��������� � ���������������?
    QString imgFile = modelGallery->record(ui->lVGallery->currentIndex().row()).value("filename").toString() ;
    qDebug() << imgFile;
    imgFile.prepend("/images/");
    imgFile.prepend(dbPath/*QApplication::applicationDirPath()*/);
    if ( !QFile::exists( imgFile )  || imgFile.isEmpty())
    {
        imgFile = ":/ico/not_found.png";
//        imgFile = "stock_people.png";
//        imgFile.prepend("/images/");
//        imgFile.prepend(QApplication::applicationDirPath());
    }
    QPixmap pixmap( imgFile );
    pixmap = pixmap.scaled(800,600);
    lblImage->resize(pixmap.size());
    lblImage->setPixmap(pixmap);
//    lblImage->showMaximized();
    lblImage->show();
}

void MainWindow::updatePhoto(const QString& fileName)//draw photo
{
    QString imgFile(fileName);
//    qDebug() << "photo Chenged" << imgFile;
    imgFile.prepend("/images/");
    imgFile.prepend(dbPath/*QApplication::applicationDirPath()*/);
    if ( !QFile::exists( imgFile ) || fileName.isEmpty())
    {
        imgFile = ":/ico/not_found.png";
//        imgFile = "stock_people.png";
//        imgFile.prepend("/images/");
//        imgFile.prepend(QApplication::applicationDirPath());
    }
//    qDebug() << imgFile;
    QPixmap pixmap( imgFile );
    if(!pixmap.isNull())
    {
        pixmap = pixmap.scaledToHeight(90);
    //    ui->wgtUserPhoto->render(&pixmap);
        ui->lblPhoto->resize(160,120);
        ui->lblPhoto->setPixmap(pixmap);
    }
}

//void MainWindow::Imtopt_from_SQL()
//{

//}

//void MainWindow::Export_to_SQL()
//{

//}

void MainWindow::selectCity(const QModelIndex &index)
{
//    qDebug() << modelG->record(index.row()).value("id").toInt();
//    saveAll();
    modelTableA->submitAll();
    modelTableA->setFilter(QString("idcity = %1").arg(modelG->record(index.row()).value("id").toInt()));

    ui->actionEditCity->setEnabled(true);
    ui->actionDelCity->setEnabled(true);
    ui->actionAddUser->setEnabled(true);

    ui->actionDelUser->setEnabled(false);
    ui->actionEditUser->setEnabled(false);
    ui->actionAddPhoto->setEnabled(false);
    ui->actionDelPhoto->setEnabled(false);
    ui->actionViewPhoto->setEnabled(false);
    ui->actionSetPhoto->setEnabled(false);
    modelGallery->setFilter("1=2");
    ui->toolBar_3->setEnabled(false);
    ui->tEDescription->clear();
    ui->lEfio->clear();
    emit updatePhoto("");
}

void MainWindow::selectUser(const QModelIndex &index)
{
//    qDebug() << modelTableA->record(index.row()).value("id").toInt();
//    saveAll();
//    modelGallery->submitAll();
    modelGallery->setFilter(QString("idabonent = %1").arg(modelTableA->record(index.row()).value("id").toInt()));
    ui->actionDelUser->setEnabled(true);
    ui->actionEditUser->setEnabled(true);
    ui->actionAddPhoto->setEnabled(true);
    ui->actionDelPhoto->setEnabled(false);
    ui->actionViewPhoto->setEnabled(false);
    ui->actionSetPhoto->setEnabled(false);
    emit updatePhoto( modelTableA->record(index.row()).value("img").toString() );
    ui->toolBar_3->setEnabled(true);
//    curentIndex = ui->lVUsers->currentIndex().row();
}

void MainWindow::selectGallery(const QModelIndex &index)
{
    ui->actionDelPhoto->setEnabled(true);
    ui->actionViewPhoto->setEnabled(true);
    ui->actionSetPhoto->setEnabled(true);
}

void MainWindow::showAbout()
{
    DialogAbout d;
    d.setTitle(tr("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt; color:#0000ff;\">About Data Base author</span></p></body></html>"));
    d.setText(tr("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">Organization: Pogranicniq local library.</span></p>\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">site: www.pogramb.ru</span></p>\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">Author: HaifLive</span></p>\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">2011 year</span></p></body></html>"));
    d.exec();
}

void MainWindow::showAuthor()
{
    DialogAbout d;
    d.setTitle(tr("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt; color:#0000ff;\">About program</span></p></body></html>"));
    d.setText(tr("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">Organization: Pogranicniq local library.</span></p>\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">site: www.pogramb.ru</span></p>\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">Author: HaifLive</span></p>\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">2011 year</span></p></body></html>"));
    d.exec();
}

void MainWindow::newDataBase()
{
//    QFileDialog
//    QFile file(dbPath + "/" + dbName);
//    if (!file.open(QIODevice::ReadWrite))
//    {
//        qDebug() << "file not readable of writeble";
//        return;
//    }

//    createConnection();
}

void MainWindow::openDatabase()
{
    QString fileName;
    fileName = QFileDialog::getOpenFileName(this,
         tr("Open Database"), dbPath, tr("dabase (%1)").arg(dbName));
    if(!fileName.isEmpty())
    {
        QFileInfo file(fileName);
        if(file.isReadable() && file.isWritable())
        {
            dbPath = file.canonicalPath();//.absoluteDir().absoluteFilePath(fileName);//file.absoluteDir().absoluteFilePath("");
        }
        else qDebug() << "can not write to file";

        saveAll();
        modelGallery->setPath(dbPath);
        createConnection();

        ui->actionEditCity->setEnabled(false);
        ui->actionDelCity->setEnabled(false);
        ui->actionAddUser->setEnabled(false);
        ui->actionDelUser->setEnabled(false);
        ui->actionEditUser->setEnabled(false);
        ui->actionAddPhoto->setEnabled(false);
        ui->actionDelPhoto->setEnabled(false);
        ui->actionViewPhoto->setEnabled(false);
        ui->actionSetPhoto->setEnabled(false);

        modelTableA->setFilter("1=2");
        modelGallery->setFilter("1=2");

        ui->toolBar_3->setEnabled(false);
        ui->tEDescription->clear();
        ui->lEfio->clear();
        emit updatePhoto("");

        modelG->select();
        modelTableA->select();
        modelGallery->select();
    }
}

void MainWindow::setupActions()
{
    connect( ui->actionAddCity, SIGNAL(triggered()), this ,SLOT(AddCity()));
    connect( ui->actionDelCity, SIGNAL(triggered()), this ,SLOT(DelCity()));
    connect( ui->actionEditCity, SIGNAL(triggered()),this ,SLOT(EditCity()));
    connect( ui->actionAddUser, SIGNAL(triggered()),this ,SLOT(AddUser()));
    connect( ui->actionEditUser, SIGNAL(triggered()),this ,SLOT(EditUser()));
    connect( ui->actionDelUser, SIGNAL(triggered()),this ,SLOT(DelUser()));
//    connect( ui->actionEditorUndo, SIGNAL(triggered()),this ,SLOT(EditorUndo()));
//    connect( ui->actionEditorRedo, SIGNAL(triggered()),this ,SLOT(EditorRedo()));
//    connect( ui->actionEditorCut, SIGNAL(triggered()),this ,SLOT(EditorCut()));
//    connect( ui->actionEditorCopy, SIGNAL(triggered()),this ,SLOT(EditorCopy()));
//    connect( ui->actionEditorPaste, SIGNAL(triggered()),this ,SLOT(EditorPaste()));
    connect( ui->actionEditorBold, SIGNAL(triggered()),this ,SLOT(EditorBold()));
    connect( ui->actionEditorItalic, SIGNAL(triggered()),this ,SLOT(EditorItalic()));
    connect( ui->actionEditorUnderLine, SIGNAL(triggered()),this ,SLOT(EditorUnderLine()));
//    connect( ui->actionEditorLeft, SIGNAL(triggered()),this ,SLOT(EditorLeft()));
//    connect( ui->actionEditorCenter, SIGNAL(triggered()),this ,SLOT(EditorCenter()));
//    connect( ui->actionEditorRight, SIGNAL(triggered()),this ,SLOT(EditorRight()));
//    connect( ui->actionEditorJustify, SIGNAL(triggered()),this ,SLOT(EditorJustify()));
//    connect( ui->actionNew_Database, SIGNAL(triggered()),this ,SLOT(New_Database()));
//    connect( ui->actionOpen_Database, SIGNAL(triggered(bool)),this ,SLOT(Open_Database()));
//    connect( ui->actionSave_Database, SIGNAL(triggered()),this ,SLOT(Save_Database()));
//    connect( ui->actionRevert_Database, SIGNAL(triggered()),this ,SLOT(Revert_Database()));
//    connect( ui->actionImtopt_from_SQL, SIGNAL(triggered()),this ,SLOT(Imtopt_from_SQL()));
//    connect( ui->actionExport_to_SQL, SIGNAL(triggered()),this ,SLOT(Export_to_SQL()));
    connect( ui->actionAddPhoto, SIGNAL(triggered()), this, SLOT(addPhoto()));
    connect( ui->actionDelPhoto, SIGNAL(triggered()), this, SLOT(delPhoto()));
//    connect( ui->tBChengePhoto,  SIGNAL(clicked()),   this, SLOT(editPhoto()));
    connect( ui->actionSetPhoto, SIGNAL(triggered()),this,SLOT(editPhoto()));
    connect( ui->actionViewPhoto,SIGNAL(triggered()), this, SLOT(openImageBrowser()));

    connect( ui->tEDescription->document(), SIGNAL(undoAvailable(bool)), ui->actionEditorUndo, SLOT(setEnabled(bool)));
    connect( ui->tEDescription->document(), SIGNAL(redoAvailable(bool)), ui->actionEditorRedo, SLOT(setEnabled(bool)));
    connect( ui->tEDescription, SIGNAL(copyAvailable(bool)), ui->actionEditorCopy, SLOT(setEnabled(bool)));
    connect( ui->tEDescription, SIGNAL(copyAvailable(bool)), ui->actionEditorCut, SLOT(setEnabled(bool)));
    connect( ui->actionEditorSizeLower, SIGNAL(triggered()), this, SLOT(textLower()));
    connect( ui->actionEditorSizeUpper, SIGNAL(triggered()), this, SLOT(textUpper()));

    connect( ui->actionWhat_this, SIGNAL(triggered()), this, SLOT(showAbout()));
    connect( ui->actionAbout, SIGNAL(triggered()), this, SLOT(showAuthor()));

    connect( ui->actionNewDatabase, SIGNAL(triggered()), this, SLOT(newDataBase()) );
    connect( ui->actionOpenDatabase,SIGNAL(triggered()), this, SLOT(openDatabase()));

#ifndef QT_NO_CLIPBOARD
    connect(QApplication::clipboard(), SIGNAL(dataChanged()), this, SLOT(clipboardDataChanged()));
    ui->actionEditorPaste->setEnabled(!QApplication::clipboard()->text().isEmpty());
#endif
    connect(ui->tEDescription, SIGNAL(currentCharFormatChanged(QTextCharFormat)),
                 this, SLOT(currentCharFormatChanged(QTextCharFormat)));
    connect(ui->tEDescription, SIGNAL(cursorPositionChanged()),
                 this, SLOT(cursorPositionChanged()));

    QActionGroup *grp = new QActionGroup(this);
    grp->addAction(ui->actionEditorLeft);
    grp->addAction(ui->actionEditorCenter);
    grp->addAction(ui->actionEditorRight);
    grp->addAction(ui->actionEditorJustify);
    connect(grp, SIGNAL(triggered(QAction*)), this, SLOT(textAlign(QAction*)));

//    connect(ui->actionEditorColor, SIGNAL(triggered()), this, SLOT(textColor()));

//    comboStyle = new QComboBox;
//    ui->tBFont->addWidget(comboStyle);
//    comboStyle->addItem("Markers");
//    comboStyle->addItem("Bullet List (Disc)");
//    comboStyle->addItem("Bullet List (Circle)");
//    comboStyle->addItem("Bullet List (Square)");
//    comboStyle->addItem("Ordered List (Decimal)");
//    comboStyle->addItem("Ordered List (Alpha lower)");
//    comboStyle->addItem("Ordered List (Alpha upper)");
//    comboStyle->addItem("Ordered List (Roman lower)");
//    comboStyle->addItem("Ordered List (Roman upper)");
//    connect(comboStyle, SIGNAL(activated(int)),
//            this, SLOT(textStyle(int)));

//    comboFont = new QFontComboBox;
//    ui->tBFont->addWidget(comboFont);
//    connect(comboFont, SIGNAL(activated(QString)),
//            this, SLOT(textFamily(QString)));

//    comboSize = new QComboBox;
//    comboSize->setObjectName("comboSize");
//    ui->tBFont->addWidget(comboSize);
//    comboSize->setEnabled(true);

//    QFontDatabase db;
//         foreach(int size, db.standardSizes())
//             comboSize->addItem(QString::number(size));

//     connect(comboSize, SIGNAL(activated(QString)),
//             this, SLOT(textSize(QString)));
//     comboSize->setCurrentIndex(comboSize->findText(QString::number(QApplication::font()
//                                                                    .pointSize())));
     fontChanged(ui->tEDescription->font());
     colorChanged(ui->tEDescription->textColor());
     alignmentChanged(ui->tEDescription->alignment());
}

void MainWindow::readSettings()
{
    QSettings settings; // ��������� ��������� (�� ������� ��� �����)

    // ��������� ��������� ����
    QRect rect = settings.value("Geometry", QRect(345, 175, 510, 450)).toRect();
    move( rect.topLeft() ); // ���������� ���
    resize( rect.size() );  // �������� ������ � �����
    dbPath = settings.value("dbPath", QApplication::applicationDirPath()).toString();

//    LangRu		= settings.value("LangRu",		false).toBool();	// ����
//    RemindBy	= settings.value("RemindBy",	3).toInt();		// �� ������� ���� ���������� � ��
//    FileName	= settings.value("FileName",	HomeDir + "database.db").toString(); // ���� ����

//    Style = settings.value("Style", "Plastique").toString(); // ����� ����������
//    QStringList styles = QStyleFactory::keys();
//    if ( styles.indexOf(Style) < 0 ) Style = "Plastique";
//    QApplication::setStyle( QStyleFactory::create(Style) );
//    QApplication::setPalette( QApplication::style()->standardPalette() );

//    SortBy = settings.value("SortBy", 1).toInt(); // �������� ����������
//    if ( (SortBy != 0) and (SortBy != 1) ) SortBy = 1;

//    NameWidth = settings.value("NameWidth", 220).toInt(); // ������ ������� � ������
//    Table->setColumnWidth( 0, NameWidth );

//    #ifdef Q_OS_WIN
//    // ������ ������ �� ������������
//    QSettings* AutorunSetting = new QSettings( "HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat );
//    QString path1 = QDir::toNativeSeparators(QCoreApplication::applicationFilePath()) + " -r";
//    QString path2 = AutorunSetting->value("Birthdays", "").toString();
//    path1 == path2 ? Remind = true : Remind = false; // ���� ������ � ������� ��������� � ���� � ���������, �� ���������� � �� �����
//    #endif
}

void MainWindow::writeSettings()
{
    QSettings settings; // ��������� ��������� ��� ������ (������ ��� ����)

    settings.setValue("Geometry",	geometry());	// ��������� ����
//    settings.setValue("dbPath",	dbPath);	// ��������� ����
//    settings.setValue("SortBy",		SortBy);		// ������� ����������
//    settings.setValue("RemindBy",	RemindBy);		// �� ������� ���� ���������� � ��
//    settings.setValue("LangRu",		LangRu);		// ���� ����������
//    settings.setValue("NameWidth",	Table->columnWidth(0)); // ������ ������� �����
//    settings.setValue("FileName",	FileName);		// ���� ����
//    settings.setValue("Style",		Style);			// ����� ����������

//    #ifdef Q_OS_WIN
//    // ������������ ��� ��������� �� ������������
//    QSettings* AutorunSetting = new QSettings( "HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat );
//    QString path = QDir::toNativeSeparators(QCoreApplication::applicationFilePath()) + " -r";
//    if ( Remind	) AutorunSetting->setValue( "Birthdays", path ); // ���� ����� ����������, �� ������������ � �������
//    else
//    {
//            // � ���� �� �����, �� ������ �������� - ��� �� ��������� ��������� ���������
//            // ���� ���, ��� � � �������� ��������, �� ����� �� �������� ���������
//            if ( path == AutorunSetting->value("Birthdays", "").toString() ) AutorunSetting->remove( "Birthdays" );
//    }
//    #endif
}

void MainWindow::saveAll()
{
    setFocus();
    modelG->submitAll();
    modelTableA->submitAll();
    modelGallery->submitAll();
}

//void MainWindow::setModified(bool ismodifed)
//{
//    modifed = ismodifed;
//}

//bool MainWindow::getModified()
//{
//    return modifed;
//}

void MainWindow::createConnection()
{
    // �������� ����������� ������ ��, ��� ������ � ���������� ����������� �� ����
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    QString filePath = /*QApplication::applicationDirPath()*/ dbPath + "/" + dbName;
    db.setDatabaseName(filePath);
    if (!db.open())
    {
        qDebug() << db.lastError().text();
    }
//        QSqlQuery query1 ("INSERT INTO abonents (idcity,fio,img,description) VALUES(1,'fio2','img/path','longhtmlfile')" );
//        query1.exec();
//        QSqlQuery query1 ("select * from abonents" );
//        query1.exec();
//        while (query1.next())
//        {
//            qDebug() << query1.value(0) << query1.value(1) << query1.value(2) << query1.value(3) << query1.value(4);
//        }
//        qDebug() << filePath << query1.lastError();
}

//void MainWindow::saveFile()
//{

//}

//void MainWindow::setDBOpen(bool open)
//{
//    isdbOpen = open;
//}

//bool MainWindow::getDBOpen()
//{
//    return isdbOpen;
//}

void MainWindow::mergeFormatOnWordOrSelection(const QTextCharFormat &format)
{
    QTextCursor cursor = ui->tEDescription->textCursor();
    if (!cursor.hasSelection())
        cursor.select(QTextCursor::WordUnderCursor);
    cursor.mergeCharFormat(format);
    ui->tEDescription->mergeCurrentCharFormat(format);
//    ui->lVUsers->setCurrentIndex(modelTableA->index(curentIndex,2));
}

void MainWindow::fontChanged(const QFont &f)
{
//    comboFont->setCurrentIndex(comboFont->findText(QFontInfo(f).family()));
//    comboSize->setCurrentIndex(comboSize->findText(QString::number(f.pointSize())));
    QFontDatabase db;
    for(int i = 0; db.standardSizes().size() > i; ++i)
        if(db.standardSizes().at(i) == f.pointSize())
            fontSize = i;
//    qDebug() << fontSize << f.pointSize();
    ui->actionEditorBold->setChecked(f.bold());
    ui->actionEditorItalic->setChecked(f.italic());
    ui->actionEditorUnderLine->setChecked(f.underline());
}

void MainWindow::colorChanged(const QColor &c)
{
    QPixmap pix(16, 16);
    pix.fill(c);
    ui->actionEditorColor->setIcon(pix);
}

void MainWindow::alignmentChanged(Qt::Alignment a)
{
    if (a & Qt::AlignLeft) {
        ui->actionEditorLeft->setChecked(true);
    } else if (a & Qt::AlignHCenter) {
        ui->actionEditorCenter->setChecked(true);
    } else if (a & Qt::AlignRight) {
        ui->actionEditorRight->setChecked(true);
    } else if (a & Qt::AlignJustify) {
        ui->actionEditorJustify->setChecked(true);
    }
}

int MainWindow::getFontSize()
{
    QFontDatabase db;
//         foreach(int size, db.standardSizes())
//             comboSize->addItem(QString::number(size));
    if(fontSize >= 18)
        fontSize = 17;
    else if(fontSize < 0)
        fontSize = 0;
    for(int i = 0; db.standardSizes().size() > i; ++i)
        if(fontSize == i)
            return db.standardSizes().at(i);
//        if(i < 18)
    return 0;
}

void MainWindow::removePhoto()
{
    QString imgFile = modelGallery->record(ui->lVGallery->currentIndex().row()).value("filename").toString() ;
    imgFile.prepend("/images/");
    imgFile.prepend(dbPath/*QApplication::applicationDirPath()*/);

    if ( QFile::exists( imgFile )  || !imgFile.isEmpty())
        QFile::remove(imgFile);
}

const QString MainWindow::dbName = "database.db";

const QString MainWindow::dbImageFilder = "images";
