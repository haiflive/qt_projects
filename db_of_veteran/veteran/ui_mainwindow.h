/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Sun 27. Mar 21:13:11 2011
**      by: Qt User Interface Compiler version 4.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QSpacerItem>
#include <QtGui/QSplitter>
#include <QtGui/QStatusBar>
#include <QtGui/QTextEdit>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionAddCity;
    QAction *actionDelCity;
    QAction *actionEditCity;
    QAction *actionAddUser;
    QAction *actionEditUser;
    QAction *actionDelUser;
    QAction *actionEditorUndo;
    QAction *actionEditorRedo;
    QAction *actionEditorCut;
    QAction *actionEditorCopy;
    QAction *actionEditorPaste;
    QAction *actionEditorBold;
    QAction *actionEditorItalic;
    QAction *actionEditorUnderLine;
    QAction *actionEditorLeft;
    QAction *actionEditorCenter;
    QAction *actionEditorRight;
    QAction *actionEditorJustify;
    QAction *actionAbout;
    QAction *actionWhat_this;
    QAction *actionLog_file;
    QAction *actionExit;
    QAction *actionAddPhoto;
    QAction *actionDelPhoto;
    QAction *actionEditorColor;
    QAction *actionEditorSizeUpper;
    QAction *actionEditorSizeLower;
    QAction *actionViewPhoto;
    QAction *actionSetPhoto;
    QAction *actionNewDatabase;
    QAction *actionOpenDatabase;
    QWidget *centralWidget;
    QGridLayout *gridLayout_4;
    QSplitter *splitter_3;
    QWidget *widget_2;
    QGridLayout *gridLayout;
    QSplitter *splitter;
    QListView *lVCites;
    QListView *lVUsers;
    QWidget *widget_3;
    QVBoxLayout *verticalLayout;
    QWidget *widget;
    QGridLayout *gridLayout_5;
    QWidget *widget_4;
    QGridLayout *gridLayout_3;
    QVBoxLayout *verticalLayout_3;
    QLabel *labelFIO;
    QLineEdit *lEfio;
    QSpacerItem *verticalSpacer_2;
    QWidget *wgtUserPhoto;
    QGridLayout *gridLayout_2;
    QLabel *lblPhoto;
    QSplitter *splitter_2;
    QTextEdit *tEDescription;
    QListView *lVGallery;
    QMenuBar *menuBar;
    QMenu *menuRecords;
    QMenu *menuEditor;
    QMenu *menuHelp;
    QMenu *menuBase;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QToolBar *toolBar;
    QToolBar *toolBar_2;
    QToolBar *toolBar_3;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(804, 415);
        actionAddCity = new QAction(MainWindow);
        actionAddCity->setObjectName(QString::fromUtf8("actionAddCity"));
        actionAddCity->setEnabled(false);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/ico/add_group.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAddCity->setIcon(icon);
        actionDelCity = new QAction(MainWindow);
        actionDelCity->setObjectName(QString::fromUtf8("actionDelCity"));
        actionDelCity->setEnabled(false);
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/ico/delete_group.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionDelCity->setIcon(icon1);
        actionEditCity = new QAction(MainWindow);
        actionEditCity->setObjectName(QString::fromUtf8("actionEditCity"));
        actionEditCity->setEnabled(false);
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/ico/edit_group.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEditCity->setIcon(icon2);
        actionAddUser = new QAction(MainWindow);
        actionAddUser->setObjectName(QString::fromUtf8("actionAddUser"));
        actionAddUser->setEnabled(false);
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/ico/add_buddy.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAddUser->setIcon(icon3);
        actionEditUser = new QAction(MainWindow);
        actionEditUser->setObjectName(QString::fromUtf8("actionEditUser"));
        actionEditUser->setEnabled(false);
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/ico/edit_buddy.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEditUser->setIcon(icon4);
        actionDelUser = new QAction(MainWindow);
        actionDelUser->setObjectName(QString::fromUtf8("actionDelUser"));
        actionDelUser->setEnabled(false);
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/ico/delete_body.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionDelUser->setIcon(icon5);
        actionEditorUndo = new QAction(MainWindow);
        actionEditorUndo->setObjectName(QString::fromUtf8("actionEditorUndo"));
        actionEditorUndo->setEnabled(false);
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/ico/edit-undo.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEditorUndo->setIcon(icon6);
        actionEditorRedo = new QAction(MainWindow);
        actionEditorRedo->setObjectName(QString::fromUtf8("actionEditorRedo"));
        actionEditorRedo->setEnabled(false);
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/ico/edit-redo.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEditorRedo->setIcon(icon7);
        actionEditorCut = new QAction(MainWindow);
        actionEditorCut->setObjectName(QString::fromUtf8("actionEditorCut"));
        actionEditorCut->setEnabled(false);
        QIcon icon8;
        icon8.addFile(QString::fromUtf8(":/ico/Cut.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEditorCut->setIcon(icon8);
        actionEditorCopy = new QAction(MainWindow);
        actionEditorCopy->setObjectName(QString::fromUtf8("actionEditorCopy"));
        actionEditorCopy->setEnabled(false);
        QIcon icon9;
        icon9.addFile(QString::fromUtf8(":/ico/Copy.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEditorCopy->setIcon(icon9);
        actionEditorPaste = new QAction(MainWindow);
        actionEditorPaste->setObjectName(QString::fromUtf8("actionEditorPaste"));
        actionEditorPaste->setEnabled(false);
        QIcon icon10;
        icon10.addFile(QString::fromUtf8(":/ico/Paste.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEditorPaste->setIcon(icon10);
        actionEditorBold = new QAction(MainWindow);
        actionEditorBold->setObjectName(QString::fromUtf8("actionEditorBold"));
        actionEditorBold->setCheckable(true);
        actionEditorBold->setEnabled(true);
        QIcon icon11;
        icon11.addFile(QString::fromUtf8(":/ico/format-text-bold.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEditorBold->setIcon(icon11);
        actionEditorItalic = new QAction(MainWindow);
        actionEditorItalic->setObjectName(QString::fromUtf8("actionEditorItalic"));
        actionEditorItalic->setCheckable(true);
        actionEditorItalic->setEnabled(true);
        QIcon icon12;
        icon12.addFile(QString::fromUtf8(":/ico/format-text-italic.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEditorItalic->setIcon(icon12);
        actionEditorUnderLine = new QAction(MainWindow);
        actionEditorUnderLine->setObjectName(QString::fromUtf8("actionEditorUnderLine"));
        actionEditorUnderLine->setCheckable(true);
        actionEditorUnderLine->setEnabled(true);
        QIcon icon13;
        icon13.addFile(QString::fromUtf8(":/ico/format-text-underline.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEditorUnderLine->setIcon(icon13);
        actionEditorLeft = new QAction(MainWindow);
        actionEditorLeft->setObjectName(QString::fromUtf8("actionEditorLeft"));
        actionEditorLeft->setCheckable(true);
        actionEditorLeft->setEnabled(true);
        QIcon icon14;
        icon14.addFile(QString::fromUtf8(":/ico/format-justify-left.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEditorLeft->setIcon(icon14);
        actionEditorCenter = new QAction(MainWindow);
        actionEditorCenter->setObjectName(QString::fromUtf8("actionEditorCenter"));
        actionEditorCenter->setCheckable(true);
        actionEditorCenter->setEnabled(true);
        QIcon icon15;
        icon15.addFile(QString::fromUtf8(":/ico/format-justify-center.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEditorCenter->setIcon(icon15);
        actionEditorRight = new QAction(MainWindow);
        actionEditorRight->setObjectName(QString::fromUtf8("actionEditorRight"));
        actionEditorRight->setCheckable(true);
        actionEditorRight->setEnabled(true);
        QIcon icon16;
        icon16.addFile(QString::fromUtf8(":/ico/format-justify-right.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEditorRight->setIcon(icon16);
        actionEditorJustify = new QAction(MainWindow);
        actionEditorJustify->setObjectName(QString::fromUtf8("actionEditorJustify"));
        actionEditorJustify->setCheckable(true);
        actionEditorJustify->setEnabled(true);
        QIcon icon17;
        icon17.addFile(QString::fromUtf8(":/ico/format-justify-fill.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEditorJustify->setIcon(icon17);
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        actionWhat_this = new QAction(MainWindow);
        actionWhat_this->setObjectName(QString::fromUtf8("actionWhat_this"));
        actionLog_file = new QAction(MainWindow);
        actionLog_file->setObjectName(QString::fromUtf8("actionLog_file"));
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        actionAddPhoto = new QAction(MainWindow);
        actionAddPhoto->setObjectName(QString::fromUtf8("actionAddPhoto"));
        actionAddPhoto->setCheckable(false);
        actionAddPhoto->setEnabled(false);
        QIcon icon18;
        icon18.addFile(QString::fromUtf8(":/ico/AddImage.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAddPhoto->setIcon(icon18);
        actionDelPhoto = new QAction(MainWindow);
        actionDelPhoto->setObjectName(QString::fromUtf8("actionDelPhoto"));
        actionDelPhoto->setCheckable(false);
        actionDelPhoto->setEnabled(false);
        QIcon icon19;
        icon19.addFile(QString::fromUtf8(":/ico/DelImage.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionDelPhoto->setIcon(icon19);
        actionEditorColor = new QAction(MainWindow);
        actionEditorColor->setObjectName(QString::fromUtf8("actionEditorColor"));
        QIcon icon20;
        icon20.addFile(QString::fromUtf8(":/ico/gtk-color-picker.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEditorColor->setIcon(icon20);
        actionEditorSizeUpper = new QAction(MainWindow);
        actionEditorSizeUpper->setObjectName(QString::fromUtf8("actionEditorSizeUpper"));
        QIcon icon21;
        icon21.addFile(QString::fromUtf8(":/ico/upper.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEditorSizeUpper->setIcon(icon21);
        actionEditorSizeLower = new QAction(MainWindow);
        actionEditorSizeLower->setObjectName(QString::fromUtf8("actionEditorSizeLower"));
        QIcon icon22;
        icon22.addFile(QString::fromUtf8(":/ico/lower.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEditorSizeLower->setIcon(icon22);
        actionViewPhoto = new QAction(MainWindow);
        actionViewPhoto->setObjectName(QString::fromUtf8("actionViewPhoto"));
        actionViewPhoto->setEnabled(false);
        QIcon icon23;
        icon23.addFile(QString::fromUtf8(":/ico/viewPhoto.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionViewPhoto->setIcon(icon23);
        actionSetPhoto = new QAction(MainWindow);
        actionSetPhoto->setObjectName(QString::fromUtf8("actionSetPhoto"));
        actionSetPhoto->setEnabled(false);
        QIcon icon24;
        icon24.addFile(QString::fromUtf8(":/ico/EditImage.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSetPhoto->setIcon(icon24);
        actionNewDatabase = new QAction(MainWindow);
        actionNewDatabase->setObjectName(QString::fromUtf8("actionNewDatabase"));
        actionOpenDatabase = new QAction(MainWindow);
        actionOpenDatabase->setObjectName(QString::fromUtf8("actionOpenDatabase"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout_4 = new QGridLayout(centralWidget);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        splitter_3 = new QSplitter(centralWidget);
        splitter_3->setObjectName(QString::fromUtf8("splitter_3"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(splitter_3->sizePolicy().hasHeightForWidth());
        splitter_3->setSizePolicy(sizePolicy);
        splitter_3->setOrientation(Qt::Horizontal);
        widget_2 = new QWidget(splitter_3);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(100);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(widget_2->sizePolicy().hasHeightForWidth());
        widget_2->setSizePolicy(sizePolicy1);
        widget_2->setMinimumSize(QSize(100, 0));
        gridLayout = new QGridLayout(widget_2);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        splitter = new QSplitter(widget_2);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setEnabled(true);
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(splitter->sizePolicy().hasHeightForWidth());
        splitter->setSizePolicy(sizePolicy2);
        splitter->setMinimumSize(QSize(0, 0));
        splitter->setOrientation(Qt::Vertical);
        lVCites = new QListView(splitter);
        lVCites->setObjectName(QString::fromUtf8("lVCites"));
        splitter->addWidget(lVCites);
        lVUsers = new QListView(splitter);
        lVUsers->setObjectName(QString::fromUtf8("lVUsers"));
        splitter->addWidget(lVUsers);

        gridLayout->addWidget(splitter, 0, 0, 1, 1);

        splitter_3->addWidget(widget_2);
        widget_3 = new QWidget(splitter_3);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        QSizePolicy sizePolicy3(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy3.setHorizontalStretch(255);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(widget_3->sizePolicy().hasHeightForWidth());
        widget_3->setSizePolicy(sizePolicy3);
        verticalLayout = new QVBoxLayout(widget_3);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        widget = new QWidget(widget_3);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setMinimumSize(QSize(0, 120));
        widget->setMaximumSize(QSize(16777215, 120));
        gridLayout_5 = new QGridLayout(widget);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        widget_4 = new QWidget(widget);
        widget_4->setObjectName(QString::fromUtf8("widget_4"));
        QSizePolicy sizePolicy4(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy4.setHorizontalStretch(1);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(widget_4->sizePolicy().hasHeightForWidth());
        widget_4->setSizePolicy(sizePolicy4);
        widget_4->setMinimumSize(QSize(0, 0));
        gridLayout_3 = new QGridLayout(widget_4);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        labelFIO = new QLabel(widget_4);
        labelFIO->setObjectName(QString::fromUtf8("labelFIO"));
        labelFIO->setMinimumSize(QSize(120, 0));

        verticalLayout_3->addWidget(labelFIO);

        lEfio = new QLineEdit(widget_4);
        lEfio->setObjectName(QString::fromUtf8("lEfio"));

        verticalLayout_3->addWidget(lEfio);

        verticalSpacer_2 = new QSpacerItem(20, 34, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_2);


        gridLayout_3->addLayout(verticalLayout_3, 0, 0, 1, 1);


        gridLayout_5->addWidget(widget_4, 0, 1, 1, 1);

        wgtUserPhoto = new QWidget(widget);
        wgtUserPhoto->setObjectName(QString::fromUtf8("wgtUserPhoto"));
        QSizePolicy sizePolicy5(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy5.setHorizontalStretch(10);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(wgtUserPhoto->sizePolicy().hasHeightForWidth());
        wgtUserPhoto->setSizePolicy(sizePolicy5);
        wgtUserPhoto->setMinimumSize(QSize(160, 120));
        wgtUserPhoto->setMaximumSize(QSize(160, 120));
        wgtUserPhoto->setLayoutDirection(Qt::LeftToRight);
        wgtUserPhoto->setStyleSheet(QString::fromUtf8(""));
        gridLayout_2 = new QGridLayout(wgtUserPhoto);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        lblPhoto = new QLabel(wgtUserPhoto);
        lblPhoto->setObjectName(QString::fromUtf8("lblPhoto"));

        gridLayout_2->addWidget(lblPhoto, 0, 0, 1, 1);


        gridLayout_5->addWidget(wgtUserPhoto, 0, 0, 1, 1);


        verticalLayout->addWidget(widget);

        splitter_2 = new QSplitter(widget_3);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setOrientation(Qt::Vertical);
        tEDescription = new QTextEdit(splitter_2);
        tEDescription->setObjectName(QString::fromUtf8("tEDescription"));
        QSizePolicy sizePolicy6(QSizePolicy::Expanding, QSizePolicy::Maximum);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(200);
        sizePolicy6.setHeightForWidth(tEDescription->sizePolicy().hasHeightForWidth());
        tEDescription->setSizePolicy(sizePolicy6);
        tEDescription->setMinimumSize(QSize(0, 0));
        splitter_2->addWidget(tEDescription);
        lVGallery = new QListView(splitter_2);
        lVGallery->setObjectName(QString::fromUtf8("lVGallery"));
        lVGallery->setEnabled(true);
        QSizePolicy sizePolicy7(QSizePolicy::Expanding, QSizePolicy::Minimum);
        sizePolicy7.setHorizontalStretch(0);
        sizePolicy7.setVerticalStretch(50);
        sizePolicy7.setHeightForWidth(lVGallery->sizePolicy().hasHeightForWidth());
        lVGallery->setSizePolicy(sizePolicy7);
        lVGallery->setMinimumSize(QSize(0, 100));
        lVGallery->setBaseSize(QSize(0, 0));
        lVGallery->setMovement(QListView::Static);
        lVGallery->setFlow(QListView::LeftToRight);
        lVGallery->setResizeMode(QListView::Fixed);
        splitter_2->addWidget(lVGallery);

        verticalLayout->addWidget(splitter_2);

        splitter_3->addWidget(widget_3);

        gridLayout_4->addWidget(splitter_3, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 804, 21));
        menuRecords = new QMenu(menuBar);
        menuRecords->setObjectName(QString::fromUtf8("menuRecords"));
        menuEditor = new QMenu(menuBar);
        menuEditor->setObjectName(QString::fromUtf8("menuEditor"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        menuBase = new QMenu(menuBar);
        menuBase->setObjectName(QString::fromUtf8("menuBase"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        mainToolBar->setEnabled(true);
        mainToolBar->setToolButtonStyle(Qt::ToolButtonIconOnly);
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QString::fromUtf8("toolBar"));
        toolBar->setEnabled(true);
        toolBar->setToolButtonStyle(Qt::ToolButtonIconOnly);
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);
        toolBar_2 = new QToolBar(MainWindow);
        toolBar_2->setObjectName(QString::fromUtf8("toolBar_2"));
        toolBar_2->setToolButtonStyle(Qt::ToolButtonIconOnly);
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar_2);
        toolBar_3 = new QToolBar(MainWindow);
        toolBar_3->setObjectName(QString::fromUtf8("toolBar_3"));
        toolBar_3->setEnabled(false);
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar_3);

        menuBar->addAction(menuBase->menuAction());
        menuBar->addAction(menuRecords->menuAction());
        menuBar->addAction(menuEditor->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuRecords->addAction(actionAddCity);
        menuRecords->addAction(actionDelCity);
        menuRecords->addAction(actionEditCity);
        menuRecords->addSeparator();
        menuRecords->addAction(actionAddUser);
        menuRecords->addAction(actionEditUser);
        menuRecords->addAction(actionDelUser);
        menuRecords->addSeparator();
        menuRecords->addAction(actionAddPhoto);
        menuRecords->addAction(actionDelPhoto);
        menuEditor->addAction(actionEditorUndo);
        menuEditor->addAction(actionEditorRedo);
        menuEditor->addAction(actionEditorCut);
        menuEditor->addAction(actionEditorCopy);
        menuEditor->addAction(actionEditorPaste);
        menuEditor->addSeparator();
        menuEditor->addAction(actionEditorBold);
        menuEditor->addAction(actionEditorItalic);
        menuEditor->addAction(actionEditorUnderLine);
        menuEditor->addAction(actionEditorSizeUpper);
        menuEditor->addAction(actionEditorSizeLower);
        menuEditor->addSeparator();
        menuEditor->addAction(actionEditorLeft);
        menuEditor->addAction(actionEditorCenter);
        menuEditor->addAction(actionEditorRight);
        menuEditor->addAction(actionEditorJustify);
        menuHelp->addAction(actionWhat_this);
        menuHelp->addAction(actionAbout);
        menuHelp->addSeparator();
        menuHelp->addAction(actionLog_file);
        menuBase->addAction(actionOpenDatabase);
        menuBase->addSeparator();
        menuBase->addAction(actionExit);
        mainToolBar->addAction(actionAddCity);
        mainToolBar->addAction(actionDelCity);
        mainToolBar->addAction(actionEditCity);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionAddUser);
        mainToolBar->addAction(actionDelUser);
        mainToolBar->addAction(actionEditUser);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionAddPhoto);
        mainToolBar->addAction(actionDelPhoto);
        mainToolBar->addAction(actionSetPhoto);
        mainToolBar->addAction(actionViewPhoto);
        toolBar->addAction(actionEditorBold);
        toolBar->addAction(actionEditorItalic);
        toolBar->addAction(actionEditorUnderLine);
        toolBar->addAction(actionEditorSizeUpper);
        toolBar->addAction(actionEditorSizeLower);
        toolBar_2->addAction(actionEditorLeft);
        toolBar_2->addAction(actionEditorCenter);
        toolBar_2->addAction(actionEditorRight);
        toolBar_2->addAction(actionEditorJustify);
        toolBar_3->addAction(actionEditorUndo);
        toolBar_3->addAction(actionEditorRedo);
        toolBar_3->addAction(actionEditorCut);
        toolBar_3->addAction(actionEditorCopy);
        toolBar_3->addAction(actionEditorPaste);

        retranslateUi(MainWindow);
        QObject::connect(actionEditorUndo, SIGNAL(triggered()), tEDescription, SLOT(undo()));
        QObject::connect(actionEditorRedo, SIGNAL(triggered()), tEDescription, SLOT(redo()));
        QObject::connect(actionEditorCut, SIGNAL(triggered()), tEDescription, SLOT(cut()));
        QObject::connect(actionEditorCopy, SIGNAL(triggered()), tEDescription, SLOT(copy()));
        QObject::connect(actionEditorPaste, SIGNAL(triggered()), tEDescription, SLOT(paste()));
        QObject::connect(actionExit, SIGNAL(triggered()), MainWindow, SLOT(close()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        actionAddCity->setText(QApplication::translate("MainWindow", "Add city", 0, QApplication::UnicodeUTF8));
        actionDelCity->setText(QApplication::translate("MainWindow", "Delete city", 0, QApplication::UnicodeUTF8));
        actionEditCity->setText(QApplication::translate("MainWindow", "Edit city", 0, QApplication::UnicodeUTF8));
        actionAddUser->setText(QApplication::translate("MainWindow", "Add user", 0, QApplication::UnicodeUTF8));
        actionEditUser->setText(QApplication::translate("MainWindow", "Edit user", 0, QApplication::UnicodeUTF8));
        actionDelUser->setText(QApplication::translate("MainWindow", "Delete user", 0, QApplication::UnicodeUTF8));
        actionEditorUndo->setText(QApplication::translate("MainWindow", "Undo", 0, QApplication::UnicodeUTF8));
        actionEditorRedo->setText(QApplication::translate("MainWindow", "Redo", 0, QApplication::UnicodeUTF8));
        actionEditorCut->setText(QApplication::translate("MainWindow", "Cut", 0, QApplication::UnicodeUTF8));
        actionEditorCopy->setText(QApplication::translate("MainWindow", "Copy", 0, QApplication::UnicodeUTF8));
        actionEditorPaste->setText(QApplication::translate("MainWindow", "Paste", 0, QApplication::UnicodeUTF8));
        actionEditorBold->setText(QApplication::translate("MainWindow", "Bold", 0, QApplication::UnicodeUTF8));
        actionEditorItalic->setText(QApplication::translate("MainWindow", "Italic", 0, QApplication::UnicodeUTF8));
        actionEditorUnderLine->setText(QApplication::translate("MainWindow", "Underline", 0, QApplication::UnicodeUTF8));
        actionEditorLeft->setText(QApplication::translate("MainWindow", "Left", 0, QApplication::UnicodeUTF8));
        actionEditorCenter->setText(QApplication::translate("MainWindow", "Center", 0, QApplication::UnicodeUTF8));
        actionEditorRight->setText(QApplication::translate("MainWindow", "Right", 0, QApplication::UnicodeUTF8));
        actionEditorJustify->setText(QApplication::translate("MainWindow", "Justify", 0, QApplication::UnicodeUTF8));
        actionAbout->setText(QApplication::translate("MainWindow", "About", 0, QApplication::UnicodeUTF8));
        actionWhat_this->setText(QApplication::translate("MainWindow", "What this", 0, QApplication::UnicodeUTF8));
        actionLog_file->setText(QApplication::translate("MainWindow", "Log file", 0, QApplication::UnicodeUTF8));
        actionExit->setText(QApplication::translate("MainWindow", "Exit", 0, QApplication::UnicodeUTF8));
        actionAddPhoto->setText(QApplication::translate("MainWindow", "Add Photo", 0, QApplication::UnicodeUTF8));
        actionDelPhoto->setText(QApplication::translate("MainWindow", "Delelete Photo", 0, QApplication::UnicodeUTF8));
        actionEditorColor->setText(QApplication::translate("MainWindow", "Editor Color", 0, QApplication::UnicodeUTF8));
        actionEditorSizeUpper->setText(QApplication::translate("MainWindow", "Editor Font Upper", 0, QApplication::UnicodeUTF8));
        actionEditorSizeLower->setText(QApplication::translate("MainWindow", "Editor Font Lower", 0, QApplication::UnicodeUTF8));
        actionViewPhoto->setText(QApplication::translate("MainWindow", "ViewPhoto", 0, QApplication::UnicodeUTF8));
        actionSetPhoto->setText(QApplication::translate("MainWindow", "set Photo", 0, QApplication::UnicodeUTF8));
        actionNewDatabase->setText(QApplication::translate("MainWindow", "New Database", 0, QApplication::UnicodeUTF8));
        actionOpenDatabase->setText(QApplication::translate("MainWindow", "Open Database", 0, QApplication::UnicodeUTF8));
        labelFIO->setText(QApplication::translate("MainWindow", "Title UserFio", 0, QApplication::UnicodeUTF8));
        lblPhoto->setText(QString());
        menuRecords->setTitle(QApplication::translate("MainWindow", "Records", 0, QApplication::UnicodeUTF8));
        menuEditor->setTitle(QApplication::translate("MainWindow", "Editor", 0, QApplication::UnicodeUTF8));
        menuHelp->setTitle(QApplication::translate("MainWindow", "Help", 0, QApplication::UnicodeUTF8));
        menuBase->setTitle(QApplication::translate("MainWindow", "Base", 0, QApplication::UnicodeUTF8));
        toolBar->setWindowTitle(QApplication::translate("MainWindow", "toolBar", 0, QApplication::UnicodeUTF8));
        toolBar_2->setWindowTitle(QApplication::translate("MainWindow", "toolBar_2", 0, QApplication::UnicodeUTF8));
        toolBar_3->setWindowTitle(QApplication::translate("MainWindow", "toolBar_3", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
