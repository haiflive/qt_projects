#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QtSql>
#include <QDebug>
#include <QMessageBox>
#include <QSqlQuery>
#include <QCloseEvent>
#include <QLineEdit>

#include <QSqlTableModel>
#include <QDataWidgetMapper>
#include <QPainter>

#include <QClipboard>
#include <QFontDatabase>
#include <QTextList>
#include <QFontComboBox>
#include <QColor>
#include <QColorDialog>
#include <QTextBlockFormat>
#include <QTextListFormat>
#include <QTextCursor>
#include <QTextCharFormat>
#include <QFont>
#include <QActionGroup>

#include "ui_mainwindow.h"
#include "modelgalleryicon.h"
#include "dialogabout.h"
//#include "photodelegate.h"
//#include "modegroup.h"
//#include "modelabonent.h"

//namespace Ui {
//    class MainWindow;
//}

class MainWindow : public QMainWindow, public Ui::MainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent *event);

private slots:
    void AddCity();
    void DelCity();
    void EditCity();
    void AddUser();
    void EditUser();
    void DelUser();
    void clipboardDataChanged();
    void EditorBold();
    void EditorItalic();
    void EditorUnderLine();
//    void EditorLeft();
//    void EditorCenter();
//    void EditorRight();
//    void EditorJustify();

    void currentCharFormatChanged(const QTextCharFormat &format);
    void cursorPositionChanged();
    void textColor();
    void textAlign(QAction *a);
    void textSize(const QString &p);
    void textFamily(const QString &f);
    void textStyle(int styleIndex);
    void textUpper();
    void textLower();
//    void New_Database();
//    void Open_Database();
//    void Save_Database();
//    void Revert_Database();
    void addPhoto();
    void delPhoto();
    void editPhoto();
    void openImageBrowser();
    void updatePhoto(const QString& fileName);
//    void Imtopt_from_SQL();
//    void Export_to_SQL();

    void selectCity(const QModelIndex &index);
    void selectUser(const QModelIndex &index);
    void selectGallery(const QModelIndex &index);

    void showAbout();
    void showAuthor();

    void newDataBase();
    void openDatabase();

private:
    void setupActions();
    void readSettings();
    void writeSettings();
    void saveAll();
//    void setModified(bool ismodifed);
//    bool getModified();
    void createConnection();
    void mergeFormatOnWordOrSelection(const QTextCharFormat &format);
    void fontChanged(const QFont &f);
    void colorChanged(const QColor &c);
    void alignmentChanged(Qt::Alignment a);
    int getFontSize();
    void removePhoto();
//    void saveFile();
//    void setDBOpen(bool open);
//    bool getDBOpen();
    Ui::MainWindow *ui;
    QString HomeDir;
    QString ProgramTitle;
    QSqlTableModel* modelG;
//    QSqlQueryModel* modelA;
    QSqlTableModel* modelTableA;
    QDataWidgetMapper* mapperA;
    QLineEdit* hidenLEMapperPhoto;
    modelGalleryIcon* modelGallery;

    QComboBox* comboStyle;
    QComboBox* comboSize;
    QFontComboBox* comboFont;
    QLabel* lblImage;
    int fontSize;
//    int curentIndex; //��� � ���������, ��������� ������� ������ ����� ������ �������
//    QString FileName;
    bool modifed;
    bool isdbOpen;
    QString dbPath;

    static const QString dbName;
    static const QString dbImageFilder; // � modelGalleryIcon �� ����������
};

#endif // MAINWINDOW_H
