#ifndef MODELGALLERYICON_H
#define MODELGALLERYICON_H

#include <QSqlTableModel>
#include <QModelIndex>
#include <QDebug>
#include <QFile>
#include <QString>
#include <QPixmap>
#include <QApplication>
#include <QDir>

class modelGalleryIcon : public QSqlTableModel
{
    Q_OBJECT
public:
    modelGalleryIcon();

    QVariant data(const QModelIndex &index, int role) const;
    void setPath(const QString& str);
//    Qt::ItemFlags flags(const QModelIndex &index) const;
private:
    QString dbPath;
};

#endif // MODELGALLERYICON_H
