#include "dialogabout.h"
#include "ui_dialogabout.h"

DialogAbout::DialogAbout(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogAbout)
{
    ui->setupUi(this);
}

DialogAbout::~DialogAbout()
{
    delete ui;
}

void DialogAbout::setTitle(const QString& str)
{
    ui->lbTitle->setText(str);
}

void DialogAbout::setText(const QString& str)
{
    ui->textEdit->setText(str);
}
