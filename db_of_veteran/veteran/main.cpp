#include <QtGui/QApplication>
#include <QTranslator>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.addLibraryPath(QString("./plugins/")); // ��� ���� ������ ���� ������ �� ���������
//    MyApp/myApp.exe
//    MyApp/QtCore4.dll
//    MyApp/mingwm10.dll
//    MyApp/���_���������_������������_���������� (�� �� �������)
//    MyApp/plugins/sqldrivers/qsqlite4.dll (������ ��� ������ � sqlite)
    // ��������� ������� ���������
    QTranslator tran;
    tran.load( "veteran_ru.qm", ":/" );
    a.installTranslator( &tran );
    QCoreApplication::setOrganizationName("veteran");
    QCoreApplication::setApplicationName ("Veteran's database");
    MainWindow w;
    w.show();
//    w.showFullScreen();

    return a.exec();
}
