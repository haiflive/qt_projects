#ifndef SQLCHILDTABLEMODEL_H
#define SQLCHILDTABLEMODEL_H

#include <QSqlTableModel>

class SqlChildTableModel : public QSqlTableModel
{
    Q_OBJECT
public:
    explicit SqlChildTableModel(QObject *parent = 0);
    QVariant data(const QModelIndex &index, int role) const;
    
signals:
    
public slots:
    
};

#endif // SQLCHILDTABLEMODEL_H
