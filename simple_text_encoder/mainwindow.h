#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>

#include "dialogabout.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    enum Ciphers
    {
        XOR = 1,
        ATBASH,
        CAESAR,
        VIGENER,
        RSA
    };

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void startEncode();
    void startDecode();
    void setCipher(QAction *a);
    void showAbout();
    bool saveAsCipher();
    bool saveAsKey();
    void openChipher();
    void openKey();

private:
//    void loadInfo();
    bool saveFile(const QString &fileName, const QString &text);
    QString loadFile(const QString &fileName);
    /* XOR method */
    QString XORencodeStr(const QString& str, int key);
    QString XORdecodeStr(const QString& str, int key);
    /* atbash method */
    QString atbashEncode(const QString& str);
    QString atbashDecode(const QString& str);
    /* Caesar method */
    QString CaesarEncode(const QString& str, int key);
    QString CaesarDecode(const QString& str, int key);
    /* Vigener method */
    QString VigenerEncode(const QString& str, const QString& key);
    QString VigenerDecode(const QString& str, const QString& key);
    QString getalfovite(QChar ch);
    QChar getchar(QString alphovite,QChar c);
    QChar getcharforuncrypt(QString alfovite,QChar c);
    /* RSA method */
    QString RSAencode(const QString& str, unsigned long int p, unsigned long int q);
    QString RSAdecode(const QString& str, unsigned long int p, unsigned long int q);
    bool RSAcheck(long int e, long int phi);

private:
    Ui::MainWindow *ui;
    Ciphers useCipher;
};

#endif // MAINWINDOW_H
