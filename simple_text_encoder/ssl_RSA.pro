#-------------------------------------------------
#
# Project created by QtCreator 2011-04-18T16:54:51
#
#-------------------------------------------------

QT       += core gui
#CONFIG += crypto

#LIBS += -L"C:\Qt\qca-2.0.3\lib\"
#LIBS += -lqca2

#INCLUDEPATH += "C:\Qt\qca-2.0.3\include\QtCrypto"
#INCLUDEPATH += "C:\Qt\qca-2.0.3\bin"

TARGET = ssl_RSA
TEMPLATE = app

#LINKAGE = -lqca
#CONFIG(debug, debug|release) {
#    windows:LINKAGE = -lqcad
#    mac:LINKAGE = -lqca_debug
#}
#LIBS += $$LINKAGE


SOURCES += main.cpp\
        mainwindow.cpp \
    dialogabout.cpp

HEADERS  += mainwindow.h \
    dialogabout.h

FORMS    += mainwindow.ui \
    dialogabout.ui

RESOURCES += \
    icon.qrc
