#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->widgetPass2->setEnabled(false);
/*  DEFAULT CIPHER XOR */
    ui->actionXOR->setChecked(true);
    useCipher = XOR;
    QActionGroup *grp = new QActionGroup(this);
    grp->addAction(ui->actionXOR);
    grp->addAction(ui->actionAtbash);
    grp->addAction(ui->actionCaesar);
    grp->addAction(ui->actionVigener);
    grp->addAction(ui->actionRSA);
    connect(grp,                    SIGNAL(triggered(QAction*)), this, SLOT(setCipher(QAction*)));
    connect(ui->actionStartEncode,  SIGNAL(triggered()), this, SLOT(startEncode()));
    connect(ui->actionStartDecode,  SIGNAL(triggered()), this, SLOT(startDecode()));
    connect(ui->actionAbout,        SIGNAL(triggered()), this, SLOT(showAbout()));
    connect(ui->actionSaveCipher,   SIGNAL(triggered()), this, SLOT(saveAsCipher()));
    connect(ui->actionOpenCipher,   SIGNAL(triggered()), this, SLOT(openChipher()));
    connect(ui->actionSaveKey,      SIGNAL(triggered()), this, SLOT(saveAsKey()));
    connect(ui->actionOpenKey,      SIGNAL(triggered()), this, SLOT(openKey()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::startEncode()
{
    QString strEncode;
    switch (useCipher)
    {
        default:
        case XOR:
            strEncode = XORencodeStr(ui->teSource->toPlainText(), ui->lePassword->text().toInt());
            break;
        case ATBASH:
            strEncode = atbashEncode(ui->teSource->toPlainText());
            break;
        case CAESAR:
            strEncode = CaesarEncode(ui->teSource->toPlainText(),ui->lePassword->text().toInt());
            break;
        case VIGENER:
            strEncode = VigenerEncode(ui->teSource->toPlainText(),ui->lePassword->text());
            break;
        case RSA:
            strEncode = RSAencode(ui->teSource->toPlainText(), ui->lePassword->text().toLong(), ui->lePassword2->text().toLong());
            break;
    }
    ui->teCipher->setText(strEncode);
}

void MainWindow::startDecode()
{
    QString strDecode;
    switch (useCipher)
    {
        default:
        case XOR:
            strDecode = XORdecodeStr(ui->teCipher->toPlainText(), ui->lePassword->text().toInt());
            break;
        case ATBASH:
            strDecode = atbashDecode(ui->teCipher->toPlainText());
            break;
        case CAESAR:
            strDecode = CaesarDecode(ui->teCipher->toPlainText(), ui->lePassword->text().toInt());
            break;
        case VIGENER:
            strDecode = VigenerDecode(ui->teCipher->toPlainText(), ui->lePassword->text());
            break;
        case RSA:
            strDecode = RSAdecode(ui->teCipher->toPlainText(),ui->lePassword->text().toLong(), ui->lePassword2->text().toLong());
            break;
    }
    ui->teSource->setText(strDecode);
}

void MainWindow::setCipher(QAction *a)
{
    ui->actionXOR->setChecked(false);
    ui->actionAtbash->setChecked(false);
    ui->actionCaesar->setChecked(false);
    ui->actionVigener->setChecked(false);
    ui->actionRSA->setChecked(false);
    ui->widgetPass2->setEnabled(false);

    ui->lePassword->setEnabled(false);
    if (a == ui->actionXOR)
    {
        useCipher = XOR;
        ui->actionXOR->setChecked(true);
        ui->lePassword->setEnabled(true);
        ui->lePassword->setText("3");
    }
    else if(a == ui->actionAtbash)
    {
        useCipher = ATBASH;
        ui->actionAtbash->setChecked(true);
    }
    else if(a == ui->actionCaesar)
    {
        useCipher = CAESAR;
        ui->actionCaesar->setChecked(true);
        ui->lePassword->setEnabled(true);
        ui->lePassword->setText("3");
    }
    else if(a == ui->actionVigener)
    {
        useCipher = VIGENER;
        ui->actionVigener->setChecked(true);
        ui->lePassword->setEnabled(true);
        ui->lePassword->setText("qwe");
    }
    else if(a == ui->actionRSA)
    {
        useCipher = RSA;
        ui->actionRSA->setChecked(true);
        ui->lePassword->setEnabled(true);
        /*! must be p*q > last char byte in Code */
        ui->lePassword->setText("11");//74177 for RU utf-8
        ui->widgetPass2->setEnabled(true);
        ui->lePassword2->setText("97");//99551 for RU utf-8
    }
}

void MainWindow::showAbout()
{
    DialogAbout dlg;
    dlg.exec();
}

bool MainWindow::saveAsCipher()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Cipher"), "Cipher.txt", tr("Cipher (*.txt)"));
    if (fileName.isEmpty())
        return false;

    return saveFile(fileName,ui->teCipher->toPlainText());
}

bool MainWindow::saveAsKey()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Key"), "Key.key", tr("Key (*.key)"));
    if (fileName.isEmpty())
        return false;
    if(useCipher == RSA)
        return saveFile(fileName, ui->lePassword->text() + "pas2:" + ui->lePassword2->text());
    return saveFile(fileName,ui->lePassword->text());
}

void MainWindow::openChipher()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Cipher"), "Cipher.txt", tr("Cipher (*.txt)"));
    if (!fileName.isEmpty())
        ui->teCipher->setPlainText(loadFile(fileName));
}

void MainWindow::openKey()
{
    // ����������� ����� RSA
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Key"), "Key.key", tr("Key (*.key)"));
    if (!fileName.isEmpty())
    {
        if(useCipher == RSA)
        {
            QString str(loadFile(fileName));
            QStringList strl(str.split("pas2:"));
            if(strl.size() == 2)
            {
                  ui->lePassword->setText(strl.at(0));
                  ui->lePassword2->setText(strl.at(1));
            }
        } else
            ui->lePassword->setText(loadFile(fileName));
    }
}
/*
void MainWindow::loadInfo()
{
//    useCipher
    QString fileName;
    switch (useCipher)
    {
        default:
        case XOR:
            fileName = "hlpXOR.txt";
            break;
        case ATBASH:
            fileName = "hlpATBASH.txt";
            break;
        case CAESAR:
            fileName = "hlpCAESAR.txt";
            break;
        case VIGENER:
            fileName = "hlpVIGENER.txt";
            break;
        case RSA:
            fileName = "hlpRSA.txt";
            break;
    }

    ui->tbInformation->clear();
//    QFile file(QApplication::applicationDirPath() + fileName);
    QFile file("C://QtProjects//ssl_RSA-build-desktop-Qt_4_7_4_for_Desktop_-_MinGW_4_4__Qt_SDK_________//debug//hlpATBASH.txt");
//    qDebug() << file.exists();
//    qDebug() << file.fileName();
         if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
             return;

    while (!file.atEnd())
    {
          QByteArray line = file.readLine();
          ui->tbInformation->insertPlainText(line);
    }

//    qDebug() << "end" << QApplication::applicationDirPath()+ "/" + fileName;
}
*/
bool MainWindow::saveFile(const QString &fileName, const QString &text)
{
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Application"),
                             tr("Cannot write file %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString()));
        return false;
    }

    QTextStream out(&file);

    out << text; //ui->teCipher->toPlainText();

    statusBar()->showMessage(tr("File saved"), 2000);
    return true;
}

QString MainWindow::loadFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Application"),
                             tr("Cannot read file %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString()));
        return NULL;
    }

    QTextStream in(&file);

    QString text;
    text = in.readAll();
//    ui->teCipher->setPlainText(in.readAll());

    statusBar()->showMessage(tr("File loaded"), 2000);
    return text;
}


QString MainWindow::XORencodeStr(const QString& str, int key)
{
    QByteArray arr(str.toUtf8());
    for(int i =0; i<arr.size(); i++)
        arr[i] = arr[i] ^ key;
//        arr[i] = ~arr[i]/* ^ key*/;

    return QString::fromAscii(arr.toBase64());
}

QString MainWindow::XORdecodeStr(const QString &str, int key)
{
    QByteArray arr = QByteArray::fromBase64(str.toAscii());
    for(int i =0; i<arr.size(); i++)
        arr[i] =arr[i] ^ key;

    return QString::fromUtf8(arr);
}

QString MainWindow::atbashEncode(const QString& str)
{
    QByteArray arr(str.toUtf8());
    for( int i=0; i < arr.size(); i++ )
        arr[i]=256-int(arr[i]);
    return QString::fromAscii(arr.toBase64());
}

QString MainWindow::atbashDecode(const QString& str)
{
    QByteArray arr = QByteArray::fromBase64(str.toAscii());
    for( int i=0; i < arr.size(); i++ )
        arr[i]=256-int(arr[i]);
    return QString::fromUtf8(arr);
}

QString MainWindow::CaesarEncode(const QString& str, int key)
{
    QString result = str;
    QString alf = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( int i = 0; i < str.size(); i++ )
    {
        int x = alf.indexOf(str[i]);
        int y = (x + key) % alf.length();
        if(x != -1)
             result[i] = alf[y];
    }
    return result;
}

QString MainWindow::CaesarDecode(const QString& str, int key)
{
    QString result = str;
    QString alf = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( int i = 0; i < str.size(); i++ )
    {
        int y = alf.indexOf(str[i]);
         int x = (y - key);
         while(x < 0) x = x + alf.length();
         x = x % alf.length();
        if(y != -1)
             result[i] = alf[x];
    }
    return result;
}

QString MainWindow::VigenerEncode(const QString& str, const QString& key)
{
    QString result = str;
    if(!key.isEmpty())
        for(int i=0; i<str.size(); i++)
        {
                result[i]=getchar(getalfovite(key[i%(key.size())]),str[i]);
        }
    return result;
}

QString MainWindow::VigenerDecode(const QString& str, const QString& key)
{
    QString result = str;
    if(!key.isEmpty())
        for(int i=0; i<str.size(); i++)
        {
            QString ouralfovite=getalfovite(key[i%(key.size())]);
            result[i]=getcharforuncrypt(ouralfovite,str[i]);
        }
    return result;
}

extern const QString tabl[26/*ALFO_SIZE*/ /*=27*/]={
                "abcdefghijklmnopqrstuvwxyz",
                "bcdefghijklmnopqrstuvwxyza",
                "cdefghijklmnopqrstuvwxyzab",
                "defghijklmnopqrstuvwxyzabc",
                "efghijklmnopqrstuvwxyzabcd",
                "fghijklmnopqrstuvwxyzabcde",
                "ghijklmnopqrstuvwxyzabcdef",
                "hijklmnopqrstuvwxyzabcdefg",
                "ijklmnopqrstuvwxyzabcdefgh",
                "jklmnopqrstuvwxyzabcdefghi",
                "klmnopqrstuvwxyzabcdefghij",
                "lmnopqrstuvwxyzabcdefghijk",
                "mnopqrstuvwxyzabcdefghijkl",
                "nopqrstuvwxyzabcdefghijklm",
                "opqrstuvwxyzabcdefghijklmn",
                "pqrstuvwxyzabcdefghijklmno",
                "qrstuvwxyzabcdefghijklmnop",
                "rstuvwxyzabcdefghijklmnopq",
                "stuvwxyzabcdefghijklmnopqr",
                "tuvwxyzabcdefghijklmnopqrs",
                "uvwxyzabcdefghijklmnopqrst",
                "vwxyzabcdefghijklmnopqrstu",
                "wxyzabcdefghijklmnopqrstuv",
                "xyzabcdefghijklmnopqrstuvw",
                "yzabcdefghijklmnopqrstuvwx",
                "zabcdefghijklmnopqrstuvwxy"
        };

QString MainWindow::getalfovite(QChar ch)
{
    if(tabl[0].indexOf(ch) != -1)
        for(int i=0;i<26/*ALFO_SIZE*/;i++)
        {
                if(tabl[i][0]==ch)return tabl[i];
        }
        return QString(ch);
}

QChar MainWindow::getchar(QString alphovite, QChar c)
{
    if(tabl[0].indexOf(c) != -1)
        for(int i=0;i<26/*ALFO_SIZE*/;i++)
        {
                if(tabl[0][i]==c)return alphovite[i];
        }
        return c;
}

QChar MainWindow::getcharforuncrypt(QString alfovite, QChar c)
{
    if(tabl[0].indexOf(c) != -1)
        for(int i=0;i<26/*ALFO_SIZE*/;i++)
        {
                if(alfovite[i]==c)return tabl[0][i];
        }
        return c;
}

QString MainWindow::RSAencode(const QString& str, unsigned long int p, unsigned long int q)
{
    unsigned long int phi,M,n,e,d,C/*,FLAG*/,s;

    n = p*q;// n > ~4 294 967 249 for RU utf-8 code
    phi=(p-1)*(q-1);
    e = 5;
    do
    {
        e++;
    }while(RSAcheck(e,phi));
    d = 1; //2282022199 for RU utf-8
    do
    {
        s = (d*e)%phi;
        d++;
    }while(s!=1);
    d = d-1;
//    Public Key: e,n;
//    Private Key: d,n;
//    statusBar()->showMessage(tr("20%"), 2000);
    QString arrResult;
    QByteArray arr(str.toUtf8());
    for(int j =0; j<arr.size(); j++)
    {
        C = 1;
        M = arr[j];
        for(unsigned long int i =0; i<e; i++)
            C = C * M % n;
            C = C % n;
        arrResult.append(QString::number(C));
        arrResult.append(",");
    }
    return arrResult;
}

QString MainWindow::RSAdecode(const QString& str, unsigned long int p, unsigned long int q)
{
    unsigned long int phi,M,n,e,d,C/*,FLAG*/,s;

    n = p*q;
    phi=(p-1)*(q-1);
    e = 5;
    do
    {
        e++;
    }while(RSAcheck(e,phi));
    d = 1; //2282022199 for RU utf-8
    do
    {
        s = (d*e)%phi;
        d++;
    }while(s!=1);
    d = d-1;
    //    Public Key: e,n;
    //    Private Key: d,n;
//    statusBar()->showMessage(tr("20%"), 2000);
    QByteArray arr;
    QStringList arr2 = str.split(',');
    for(int j =0; j < arr2.size(); j++)
    {
        M = 1;
        C = arr2.at(j).toInt();
        for(unsigned long int i =0; i < d; i++)
            M = M * C % n; // ��� ��������� ����������������� ��������� ���������
        M = M % n;
        arr[j] = M;
//        statusBar()->showMessage(QString::number(j / arr2.size() * 100 )+("%"), 2000);
    }

    return QString::fromUtf8(arr);
}

bool MainWindow::RSAcheck(long int e, long int phi)
{
    unsigned long int i;
    for(i=3;(e%i==0 && phi%i==0);i+2)
        return true;
    return false;
}
